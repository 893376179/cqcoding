
![logo](image/undraw_Code_thinking_re_gka2.png)


- 本站仿照r2coding，在原有基础上增加阿巩自学编程以来所用资源和分享内容的大聚合。其中包含从全网搜集到的清晰的学习路线、靠谱的资源、高效的工具、和务实的文章。**网站内容会持续保持更新，欢迎大家收藏！**

[**联系作者**](http://mp.weixin.qq.com/s?__biz=MzU3OTg2NDE0Mw==&mid=2247484799&idx=1&sn=249160dd75cb9c5ddd81c67176859ab8&chksm=fd5ed65fca295f49f82fe59858b7603808040d81b3cd19c1d7fe601208b937e1c61bd406db98&scene=18#wechat_redirect)
[**开启阅读**](README.md)