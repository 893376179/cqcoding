
<p align="center">
    <a href="http://mp.weixin.qq.com/s?__biz=MzU3OTg2NDE0Mw==&mid=2247484799&idx=1&sn=249160dd75cb9c5ddd81c67176859ab8&chksm=fd5ed65fca295f49f82fe59858b7603808040d81b3cd19c1d7fe601208b937e1c61bd406db98&scene=18#wechat_redirect" target="_blank">
        <img src="image/undraw_Code_thinking_re_gka2.png" width=""/>
    </a>
</p>




---
# **网站内容导航**
---

![logo](image/cqcoding.png)

# **Coding知识点梳理**
## Gopher之路
> - [**【Go语言基础速刷手册】阿巩爆肝整理，便于快速了解Go语言**](https://mp.weixin.qq.com/s?__biz=MzU3OTg2NDE0Mw==&mid=2247483952&idx=1&sn=017a8c1aa5be55e58682d87d36be1a63&chksm=fd5ed110ca2958060c7ed84ac28abc16c27ee6b246e99a33ec161d4f06f0bb9cbb4ff4accf25&token=2137302191&lang=zh_CN#rd)

### Go基础

- 1.**Go 程序的基本结构？**

<img src="https://image-1302243118.cos.ap-beijing.myqcloud.com/img/image-20220219155342065.png" alt="img" style="zoom:67%;" />

- 2. **Go 有哪些关键字？**

  <img src="https://image-1302243118.cos.ap-beijing.myqcloud.com/imgcdn/image-20220219155518269.png" alt="img" style="zoom: 67%;" />

- 3. **Go 有哪些数据类型？**

<img src="https://image-1302243118.cos.ap-beijing.myqcloud.com/imgcdn/image-20220219155439733.png" alt="img" style="zoom:67%;" />

- 4. **Go 方法与函数的区别？**

  > 在Go语言中，函数和方法不太一样，有明确的概念区分。其他语言中，比如Java，一般来说函数就是方法，方法就是函数；但是在Go语言中，函数是指不属于任何结构体、类型的方法，也就是说函数是没有接收者的；而方法是有接收者的

  方法

  ```go
  func (t *T) add(a, b int) int {
  	return a + b 
  }
  //其中T是自定义类型或者结构体，不能是基础数据类型int等
  ```

  函数

  ```go
  func add(a, b int) int {
  	return a + b 
  }
  ```

  

- 5.**Go 方法值接收者和指针接收者的区别?**

> 如果方法的接收者是指针类型，无论调用者是对象还是对象指针，修改的都是对象本身，会影响调用者；
>
> 如果方法的接收者是值类型，无论调用者是对象还是对象指针，修改的都是对象的副本，不影响调用者。
>
> [示例代码](https://gitlab.com/893376179/golang-basic-review/-/blob/main/value_pointer/value_pointer.go)

- 6.Go 函数参数传递到底是值传递还是引用传递？

>  **Go语言中所有的传参都是值传递**。
>
> **什么是值传递？**
>
> 将实参的值传递给形参，形参是实参的一份拷贝，实参和形参的内存地址不同。函数内对形参值内容的修改，是否会影响实参的值内容，取决于参数是否是引用类型。
>
> **什么是引用传递？**
>
> 将实参的地址传递给形参，函数内对形参值内容的修改，将会影响实参的值内容。Go语言是没有引用传递的，在C++中，函数参数的传递方式有引用传递。
>
> 下面分别针对Go的值类型（int、struct等）、引用类型（指针、slice、map、channel），验证是否是值传递，以及函数内对形参的修改是否会修改原内容数据。

- 7.Go defer关键字的实现原理？

> defer 能够让我们推迟执行某些函数调用，推迟到当前函数**返回前**才实际执行。defer与panic和recover结合，形成了Go语言风格的异常与捕获机制。defer 语句经常被用于处理成对的操作，如文件句柄关闭、连接关闭、释放锁。
>
> **实现原理**：
>
> Go1.14中编译器会将defer函数直接插入到函数的尾部，无需链表和栈上参数拷贝，性能大幅提升。把defer函数在当前函数内展开并直接调用，这种方式被称为open coded defer。
>
> [示例代码](https://gitlab.com/893376179/golang-basic-review/-/tree/main/defer)

- 8.Go 内置函数make和new的区别？

> new 和 make 两个内置函数，主要用来分配内存空间，有了内存，变量就能使用了，主要有以下2点区别：
>
> **使用场景区别：**
>
> make 只能用来分配及初始化类型为slice、map、chan 的数据。
>
> new 可以分配任意类型的数据，并且置零。
>
> **返回值区别：**
>
> make函数原型如下，返回的是slice、map、chan类型本身；
>
> 这3种类型是引用类型，就没有必要返回他们的指针。
>
> ```go
> func make(t Type, size ...IntegerType) Type
> ```
>
> 复制代码
>
> new函数原型如下，返回一个指向该类型内存地址的指针。
>
> ```go
> func new(Type) *Type
> ```

### Slice

- 1.Go slice的底层实现原理?

> 切片是基于数组实现的，它的底层是数组，可以理解为对 底层数组的抽象。
>
> 源码包中src/runtime/slice.go 定义了slice的数据结构：
>
> ```go
> type slice struct {
>     array unsafe.Pointer  // 指向底层数组的指针，占用8个字节
>     len   int  // 切片的长度，占用8个字节
>     cap   int  // 切片的容量，cap 总是大于等于 len 的，占用8个字节
> }
> ```

> slice有4种初始化方式
>
> ```go
> // 初始化方式1：直接声明
> var slice1 []int
> 
> // 初始化方式2：使用字面量
> slice2 := []int{1, 2, 3, 4}
> 
> // 初始化方式3：使用make创建slice
> slice3 := make([]int, 3, 5)         
> 
> // 初始化方式4: 从切片或数组“截取”
> slcie4 := arr[1:3]
> ```

- 2.Go slice深拷贝和浅拷贝

> 深拷贝：拷贝的是数据本身，创造一个新对象，新创建的对象与原对象不共享内存，新创建的对象在内存中开辟一个新的内存地址，新对象值修改时不会影响原对象值。
>
> 实现深拷贝的方式：
>
> 1. copy(slice2, slice1)
> 2. 遍历append赋值
>
> 浅拷贝：拷贝的是数据地址，只复制指向的对象的指针，此时新对象和老对象指向的内存地址是一样的，新对象值修改时老对象也会变化。
>
> 实现浅拷贝的方式：
>
> 引用类型的变量，默认赋值操作就是浅拷贝
>
> 1. slice2 := slice1

- 3.Go slice扩容机制？

> 扩容会发生在slice append的时候，当slice的cap不足以容纳新元素，就会进行扩容，扩容规则如下:
>
> - 如果新申请容量比两倍原有容量大，那么扩容后容量大小 为 新申请容量；
> - 如果原有 slice 长度小于 1024， 那么每次就扩容为原来的 2 倍；
> - 如果原 slice 长度大于等于 1024， 那么每次扩容就扩为原来的 1.25 倍。

- 4.Go slice为什么不是线程安全的？

> 先看下线程安全的定义：
>
> 多个线程访问同一个对象时，调用这个对象的行为都可以获得正确的结果，那么这个对象就是线程安全的。
>
> 若有多个线程同时执行写操作，一般都需要考虑线程同步，否则的话就可能影响线程安全。
>
> 再看**Go语言实现线程安全常用的几种方式**：
>
> 1. 互斥锁
> 2. 读写锁
> 3. 原子操作
> 4. sync.once
> 5. sync.atomic
> 6. channel
>
> slice底层结构并没有使用加锁等方式，不支持并发读写，所以并不是线程安全的，使用多个 goroutine 对类型为 slice 的变量进行操作，每次输出的值大概率都不会一样，与预期值不一致; slice在并发执行中不会报错，但是数据会丢失。
>
> ```go
> /**
> * 切片非并发安全
> * 多次执行，每次得到的结果都不一样
> * 可以考虑使用 channel 本身的特性 (阻塞) 来实现安全的并发读写
>  */
> func TestSliceConcurrencySafe(t *testing.T) {
>  a := make([]int, 0)
>  var wg sync.WaitGroup
>  for i := 0; i < 10000; i++ {
>   wg.Add(1)
>   go func(i int) {
>    a = append(a, i)
>    wg.Done()
>   }(i)
>  }
>  wg.Wait()
>  t.Log(len(a)) 
>  // not equal 10000
> }
> ```

### Map

- 1.Go map的底层实现原理？

> Go中的map是一个指针，占用8个字节，指向hmap结构体
>
> 源码包中`src/runtime/map.go`定义了hmap的数据结构：
>
> hmap包含若干个结构为bmap的数组，每个bmap底层都采用链表结构，bmap通常叫其bucket。
>
> <img src="https://image-1302243118.cos.ap-beijing.myqcloud.com/imgcdn/640.png" alt="图片" style="zoom:67%;" />
>
> Go map中的key 和 value 是各自放在一起的，并不是 `key/value/key/value/...` 这样的形式，当key和value类型不一样的时候，key和value占用字节大小不一样，使用key/value这种形式可能会因为内存对齐导致内存空间浪费，所以Go采用key和value分开存储的设计，更节省内存空间。

- 2.Go map遍历为什么是无序的？

> map 本身是无序的，且遍历时顺序还会被随机化，如果想顺序遍历 map，需要对 map key 先排序，再按照 key 的顺序遍历 map。
>
> 主要原因有2点：
>
> - map在遍历时，并不是从固定的0号bucket开始遍历的，每次遍历，都会从一个**随机值序号的bucket**，再从其中**随机的cell**开始遍历。
> - map遍历时，是按序遍历bucket，同时按需遍历bucket中和其overflow bucket中的cell。但是map在扩容后，会发生key的搬迁，这造成原来落在一个bucket中的key，搬迁后，有可能会落到其他bucket中了，从这个角度看，遍历map的结果就不可能是按照原来的顺序了。

- 3.Go map为什么是非线程安全的？

> map默认是并发不安全的，同时对map进行并发读写时，程序会panic。
>
> 场景: 2个协程同时读和写，以下程序会出现致命错误：fatal error: concurrent map writes
>
> ```go
> package main
> 
> import (
>     "fmt"
>     "time"
> )
> 
> func main() {
>     s := make(map[int]int)
>     for i := 0; i < 100; i++ {
>         go func(i int) {
>             s[i] = i
>         }(i)
>     }
>     for i := 0; i < 100; i++ {
>         go func(i int) {
>             fmt.Printf("map第%d个元素值是%d\n", i, s[i])
>         }(i)
>     }
>     time.Sleep(1 * time.Second)
> } // fatal error: concurrent map writes
> ```
>
> 如果想实现map线程安全，有两种方式：
>
> 方式一：使用读写锁 `map` + `sync.RWMutex`
>
> ```go
> package main
> 
> import (
>     "fmt"
>     "sync"
>     "time"
> )
> 
> func main() {
>     var lock sync.RWMutex
>     s := make(map[int]int)
>     for i := 0; i < 100; i++ {
>         go func(i int) {
>             lock.Lock()
>             s[i] = i
>             lock.Unlock()
>         }(i)
>     }
>     for i := 0; i < 100; i++ {
>         go func(i int) {
>             lock.RLock()
>             fmt.Printf("map第%d个元素值是%d\n", i, s[i])
>             lock.RUnlock()
>         }(i)
>     }
>     time.Sleep(1 * time.Second)
> }
> //map第0个元素值是0
> // map第6个元素值是6
> // map第8个元素值是8
> // map第7个元素值是7
> // map第5个元素值是5
> // …………
> ```

> 方式二：使用Go提供的 `sync.Map`
>
> ```go
> package main
> 
> import (
>     "fmt"
>     "sync"
>     "time"
> )
> 
> func main() {
>     var m sync.Map
>     for i := 0; i < 100; i++ {
>         go func(i int) {
>             m.Store(i, i)
>         }(i)
>     }
>     for i := 0; i < 100; i++ {
>         go func(i int) {
>             v, ok := m.Load(i)
>             fmt.Printf("Load: %v, %v\n", v, ok)
>         }(i)
>     }
>     time.Sleep(1 * time.Second)
> }
> // Load: 3, true
> // Load: 5, true
> // Load: 4, true
> // Load: 1, true
> // Load: 2, true
> // Load: 8, true
> // Load: 11, true
> // Load: 6, true
> // Load: 9, true
> // …………
> ```

- 4.Go map如何查找？

> Go 语言中读取 map 有两种语法：带 comma 和 不带 comma。当要查询的 key 不在 map 里，带 comma 的用法会返回一个 bool 型变量提示 key 是否在 map 中；而不带 comma 的语句则会返回一个 value 类型的零值。如果 value 是 int 型就会返回 0，如果 value 是 string 类型，就会返回空字符串。
>
> ```go
> // 不带 comma 用法
> value := m["name"]
> fmt.Printf("value:%s", value)
> 
> // 带 comma 用法
> value, ok := m["name"]
> if ok {
>     fmt.Printf("value:%s", value)
> }
> ```

- 5.Go map冲突的解决方式？

> 比较常用的Hash冲突解决方案有链地址法和开放寻址法：
>
> **链地址法**
>
> 当哈希冲突发生时，创建新**单元**，并将新单元添加到冲突单元所在链表的尾部。
>
> **开放寻址法**
>
> 当哈希冲突发生时，从发生冲突的那个**单元**起，按照一定的次序，从哈希表中寻找一个空闲的单元，然后把发生冲突的元素存入到该单元。**开放寻址法需要的表长度要大于等于所需要存放的元素数量**
>
> 在发生哈希冲突时，Python中dict采用的开放寻址法，Java的HashMap采用的是链地址法，而Go map也采用链地址法解决冲突，具体就是**插入key到map中时**，当key定位的桶**填满8个元素后**（这里的单元就是桶，不是元素），将会创建一个溢出桶，并且将溢出桶插入当前桶所在链表尾部。

- 6.Go map 的负载因子为什么是 6.5？

> **什么是负载因子?**
>
> **负载因子（load factor），用于衡量当前哈希表中空间占用率的核心指标**，也就是每个 bucket 桶存储的平均元素个数。
>
> 另外负载因子**与扩容、迁移**等重新散列（rehash）行为有直接关系：
>
> - 在程序运行时，会不断地进行插入、删除等，会导致 bucket 不均，内存利用率低，需要迁移。
> - 在程序运行时，出现负载因子过大，需要做扩容，解决 bucket 过大的问题。
>
> **装载因子越大，填入的元素越多，空间利用率就越高，但发生哈希冲突的几率就变大。反之，装载因子越小，填入的元素越少，冲突发生的几率减小，但空间浪费也会变得更多，而且还会提高扩容操作的次数**
>
> 根据这份测试结果和讨论，Go 官方取了一个相对适中的值，把 Go 中的 map 的负载因子硬编码为 6.5，这就是 6.5 的选择缘由。
>
> 这意味着在 Go 语言中，**当 map存储的元素个数大于或等于 6.5 \* 桶个数 时，就会触发扩容行为**。

- 7.Go map如何扩容?

> **扩容时机：**
>
> 在**向 map 插入新 key** 的时候，会进行条件检测，符合下面这 2 个条件，就会触发扩容：
>
> 条件1：超过负载
>
> map元素个数 > 6.5 * 桶个数。
>
> 条件2：溢出桶太多
>
> 当桶总数 < 2 ^ 15 时，如果溢出桶总数 >= 桶总数，则认为溢出桶过多；
>
> 当桶总数 >= 2 ^ 15 时，直接与 2 ^ 15 比较，当溢出桶总数 >= 2 ^ 15 时，即认为溢出桶太多了。
>
> 对于条件2，其实算是对条件1的补充。因为在负载因子比较小的情况下，有可能 map 的查找和插入效率也很低，而第 1 点识别不出来这种情况。
>
> **扩容机制：**
>
> 双倍扩容：针对条件1，新建一个buckets数组，新的buckets大小是原来的2倍，然后旧buckets数据搬迁到新的buckets。该方法我们称之为**双倍扩容**。
>
> 等量扩容：针对条件2，并不扩大容量，buckets数量维持不变，重新做一遍类似双倍扩容的搬迁动作，把松散的键值对重新排列一次，使得同一个 bucket 中的 key 排列地更紧密，节省空间，提高 bucket 利用率，进而保证更快的存取。该方法我们称之为**等量扩容**。

- 8.Go map和sync.Map谁的性能好，为什么？

> Go 语言的 `sync.Map` 支持并发读写，采取了 “空间换时间” 的机制，冗余了两个数据结构，分别是：read 和 dirty。
>
> ```go
> type Map struct {
>    mu Mutex
>    read atomic.Value // readOnly
>    dirty map[interface{}]*entry
>    misses int
> }
> ```

> **对比原始map：**
>
> 和原始map+RWLock的实现并发的方式相比，减少了加锁对性能的影响。它做了一些优化：可以无锁访问read map，而且会优先操作read map，倘若只操作read map就可以满足要求，那就不用去操作write map(dirty)，所以在某些特定场景中它发生锁竞争的频率会远远小于map+RWLock的实现方式
>
> **优点：**
>
> 适合读多写少的场景
>
> **缺点：**
>
> 写多的场景，会导致 read map 缓存失效，需要加锁，冲突变多，性能急剧下降

### Channel

- 1.Go channel的底层实现原理？

> Go中的channel 是一个队列，遵循先进先出的原则，负责协程之间的通信（Go 语言提倡不要通过共享内存来通信，而要通过通信来实现内存共享，CSP(Communicating Sequential Process)并发模型，就是通过 goroutine 和 channel 来实现的）
>
> **使用场景：**
>
> 停止信号监听
>
> 定时任务
>
> 生产方和消费方解耦
>
> 控制并发数
>
> **底层数据结构：**
>
> 通过var声明或者make函数创建的channel变量是一个存储在函数栈帧上的指针，占用8个字节，指向堆上的hchan结构体
>
> 源码包中`src/runtime/chan.go`定义了hchan的数据结构：
>
> <img src="https://image-1302243118.cos.ap-beijing.myqcloud.com/imgcdn/hchan.png" alt="hchan" style="zoom:67%;" />
>
> ```go
> type hchan struct {
>  closed   uint32   // channel是否关闭的标志
>  elemtype *_type   // channel中的元素类型
>  
>  // channel分为无缓冲和有缓冲两种。
>  // 对于有缓冲的channel存储数据，使用了 ring buffer（环形缓冲区) 来缓存写入的数据，本质是循环数组
>  // 为啥是循环数组？普通数组不行吗，普通数组容量固定更适合指定的空间，弹出元素时，普通数组需要全部都前移
>  // 当下标超过数组容量后会回到第一个位置，所以需要有两个字段记录当前读和写的下标位置
>  buf      unsafe.Pointer // 指向底层循环数组的指针（环形缓冲区）
>  qcount   uint           // 循环数组中的元素数量
>  dataqsiz uint           // 循环数组的长度
>  elemsize uint16                 // 元素的大小
>  sendx    uint           // 下一次写下标的位置
>  recvx    uint           // 下一次读下标的位置
>   
>  // 尝试读取channel或向channel写入数据而被阻塞的goroutine
>  recvq    waitq  // 读等待队列
>  sendq    waitq  // 写等待队列
> 
>  lock mutex //互斥锁，保证读写channel时不存在并发竞争问题
> }
> ```

- 2.Go channel有无缓冲的区别？

> 无缓冲：一个送信人去你家送信，你不在家他不走，你一定要接下信，他才会走。
>
> 有缓冲：一个送信人去你家送信，扔到你家的信箱转身就走，除非你的信箱满了，他必须等信箱有多余空间才会走。

|          | **无缓冲**         | **有缓冲**            |
| -------- | ------------------ | --------------------- |
| 创建方式 | make(chan TYPE)    | make(chan TYPE, SIZE) |
| 发送阻塞 | 数据接收前发送阻塞 | 缓冲满时发送阻塞      |
| 接收阻塞 | 数据发送前接收阻塞 | 缓冲空时接收阻塞      |

>**非缓冲** `channel`
>
>```go
>package main
>
>import (
>    "fmt"
>    "time"
>)
>
>func loop(ch chan int) {
>    for {
>        select {
>        case i := <-ch:
>            fmt.Println("this  value of unbuffer channel", i)
>        }
>    }
>}
>
>func main() {
>    ch := make(chan int)
>    ch <- 1  // ch<-1 发送了，但是同时没有接收者，所以就发生了阻塞
>    go loop(ch)
>    time.Sleep(1 * time.Millisecond)
>} // fatal error: all goroutines are asleep - deadlock!
>```

> **缓冲** `channel`
>
> ```go
> package main
> 
> import (
>     "fmt"
>     "time"
> )
> 
> func loop(ch chan int) {
>     for {
>         select {
>         case i := <-ch:
>             fmt.Println("this  value of unbuffer channel", i)
>         }
>     }
> }
> 
> func main() {
>     ch := make(chan int,3)
>     ch <- 1
>     ch <- 2
>     ch <- 3
>     ch <- 4
>     go loop(ch)
>     time.Sleep(1 * time.Millisecond)
> }
> ```

> 这里也会报 fatal error: all goroutines are asleep - deadlock! ，这是因为 channel 的大小为 3 ，而我们要往里面塞 4 个数据，所以就会阻塞住，解决的办法有两个:
>
> 1. 把 channel 长度调大一点。
> 2. 把 channel 的信息发送者 ch <- 1 这些代码移动到 go loop(ch) 下面 ，让 channel 实时消费就不会导致阻塞了。

- 3.Go channel为什么是线程安全的？

> **为什么设计成线程安全？**
>
> 不同协程通过channel进行通信，本身的使用场景就是多线程，为了保证数据的一致性，必须实现线程安全。
>
> **如何实现线程安全的？**
>
> channel的底层实现中，hchan结构体中采用Mutex锁来保证数据读写安全。在对循环数组buf中的数据进行入队和出队操作时，必须先获取互斥锁，才能操作channel数据。

- 4.Go channel如何控制goroutine并发执行顺序？

> **多个goroutine并发执行时，每一个goroutine抢到处理器的时间点不一致，gorouine的执行本身不能保证顺序**。即代码中先写的gorouine并不能保证先执行
>
> 思路：使用channel进行通信通知，用channel去传递信息，从而控制并发执行顺序。
>
> [代码实现](https://gitlab.com/893376179/golang-basic-review/-/blob/main/channel/channel_control_goroutine/concurrent_order.go)

- 5.Go channel共享内存有什么优劣势？

> **“不要通过共享内存来通信，我们应该使用通信来共享内存”** 这句话想必大家已经非常熟悉了，在官方的博客，初学时的教程，甚至是在 Go 的源码中都能看到
>
> 无论是通过共享内存来通信还是通过通信来共享内存，最终我们应用程序都是读取的内存当中的数据，只是前者是直接读取内存的数据，而后者是通过发送消息的方式来进行同步。而通过发送消息来同步的这种方式常见的就是 Go 采用的 CSP(Communication Sequential Process) 模型以及 Erlang 采用的 Actor 模型，这两种方式都是通过通信来共享内存。
>
> <img src="https://img.lailin.xyz/image/1610460699237-f6400aaa-34d5-4c8d-b323-27683704abd2.png" alt="02_Go进阶03_blog_channel.png" style="zoom:67%;" />

> 大部分的语言采用的都是第一种方式直接去操作内存，然后通过互斥锁，CAS 等操作来保证并发安全。Go 引入了 Channel 和 Goroutine 实现 CSP 模型将生产者和消费者进行了解耦，Channel 其实和消息队列很相似。而 Actor 模型和 CSP 模型都是通过发送消息来共享内存，但是它们之间最大的区别就是 Actor 模型当中并没有一个独立的 Channel 组件，而是 Actor 与 Actor 之间直接进行消息的发送与接收，每个 Actor 都有一个本地的“信箱”消息都会先发送到这个“信箱当中”。
>
> **优点**
>
> 使用 channel 可以帮助我们解耦生产者和消费者，可以降低并发当中的耦合
>
> **缺点**
>
> 容易出现死锁的情况

- 6.Go channel发送和接收什么情况下会死锁？

> **死锁：**
>
> - 单个协程永久阻塞
> - 两个或两个以上的协程的执行过程中，由于竞争资源或由于彼此通信而造成的一种阻塞的现象。
>
> **channel死锁场景：**
>
> - 非缓存channel只写不读
> - 非缓存channel读在写后面
> - 缓存channel写入超过缓冲区数量
> - 空读
> - 多个协程互相等待
>
> [代码实现](https://gitlab.com/893376179/golang-basic-review/-/blob/main/channel/channel_deadlock/deadlock.go)

### Mutex

- 1.Go 互斥锁的实现原理？

> Go sync包提供了两种锁类型：互斥锁sync.Mutex 和 读写互斥锁sync.RWMutex，都属于悲观锁。
>
> Mutex是互斥锁，当一个 goroutine 获得了锁后，其他 goroutine 不能获取锁（只能存在一个写者或读者，不能同时读和写）
>
> **使用场景：**
>
> 多个线程同时访问临界区，为保证数据的安全，锁住一些共享资源， 以防止并发访问这些共享数据时可能导致的数据不一致问题。
>
> 获取锁的线程可以正常访问临界区，未获取到锁的线程等待锁释放后可以尝试获取锁。
>
> **底层实现：**
>
> 互斥锁对应的是底层结构是sync.Mutex结构体，，位于 src/sync/mutex.go中。
>
> ```go
> type Mutex struct {  
>      state int32  
>      sema  uint32
>  }
> ```
>
> state表示锁的状态，有锁定、被唤醒、饥饿模式等，并且是用state的二进制位来标识的，不同模式下会有不同的处理方式。
>
> sema表示信号量，mutex阻塞队列的定位是通过这个变量来实现的，从而实现goroutine的阻塞和唤醒。
>
> **操作：**
>
> 锁的实现一般会依赖于原子操作、信号量，通过atomic 包中的一些原子操作来实现锁的锁定，通过信号量来实现线程的阻塞与唤醒。
>
> 加锁：通过原子操作cas加锁，如果加锁不成功，根据不同的场景选择自旋重试加锁或者阻塞等待被唤醒后加锁。
>
> 解锁：通过原子操作add解锁，如果仍有goroutine在等待，唤醒等待的goroutine。
>
> **注意点**：
>
> - 在 Lock() 之前使用 Unlock() 会导致 panic 异常。
> - 使用 Lock() 加锁后，再次 Lock() 会导致死锁（不支持重入），需Unlock()解锁后才能再加锁。
> - 锁定状态与 goroutine 没有关联，一个 goroutine 可以 Lock，另一个 goroutine 可以 Unlock。

- 2.Go 互斥锁正常模式和饥饿模式的区别？

> 在Go一共可以分为两种抢锁的模式，一种是**正常模式**，另外一种是**饥饿模式**。
>
> 正常模式：在刚开始的时候，是处于正常模式（Barging），也就是，当一个G1持有着一个锁的时候，G2会自旋的去尝试获取这个锁
>
> 当自旋超过4次还没有能获取到锁的时候，这个G2就会被加入到获取锁的等待队列里面，并阻塞等待唤醒
>
> 正常模式下，所有等待锁的 goroutine 按照 FIFO(先进先出)顺序等待。唤醒的goroutine 不会直接拥有锁，而是会和新请求锁的 goroutine 竞争锁。新请求锁的 goroutine 具有优势：它正在 CPU 上执行，而且可能有好几个，所以刚刚唤醒的 goroutine 有很大可能在锁竞争中失败，长时间获取不到锁，就会切换到饥饿模式。
>
> 饥饿模式：当一个 goroutine 等待锁时间超过 1 毫秒时，它可能会遇到饥饿问题。 在版本1.9中，这种场景下Go Mutex 切换到饥饿模式（handoff），解决饥饿问题。
>
> 饥饿模式回归正常模式必须具备的条件有以下几种：
>
> 1. G的执行时间小于1ms
> 2. 等待队列已经全部清空了
>
> 当满足上述两个条件的任意一个的时候，Mutex会切换回正常模式，而Go的抢锁的过程，就是在这个正常模式和饥饿模式中来回切换进行的。
>
> 对于两种模式，正常模式下的性能是最好的，goroutine 可以连续多次获取锁，饥饿模式解决了取锁公平的问题，但是性能会下降，其实是性能和公平的 一个平衡模式。

- 3.Go 互斥锁允许自旋的条件？

> 线程没有获取到锁时常见有2种处理方式：
>
> - 一种是没有获取到锁的线程就一直循环等待判断该资源是否已经释放锁，这种锁也叫做**自旋锁**，它不用将线程阻塞起来， 适用于并发低且程序执行时间短的场景，缺点是cpu占用较高。
> - 另外一种处理方式就是把自己阻塞起来，会**释放CPU给其他线程**，内核会将线程置为「睡眠」状态，等到锁被释放后，内核会在合适的时机唤醒该线程，适用于高并发场景，缺点是有线程上下文切换的开销。
>
> Go语言中的Mutex实现了自旋与阻塞两种场景，当满足不了自旋条件时，就会进入阻塞。
>
> **允许自旋的条件：**
>
> 1. 锁已被占用，并且锁不处于饥饿模式。
> 2. 积累的自旋次数小于最大自旋次数（active_spin=4）。
> 3. cpu 核数大于 1。
> 4. 有空闲的 P。
> 5. 当前 goroutine 所挂载的 P 下，本地待运行队列为空。
>
> 如果可以进入自旋状态之后就会调用 `runtime_doSpin` 方法进入自旋， `doSpin` 方法会调用 `procyield(30)` 执行30次 `PAUSE` 指令，什么都不做，但是会消耗CPU时间。

- 4.Go 读写锁的实现原理？

> 读写互斥锁RWMutex，是对Mutex的一个扩展，当一个 goroutine 获得了读锁后，其他 goroutine可以获取读锁，但不能获取写锁；当一个 goroutine 获得了写锁后，其他 goroutine既不能获取读锁也不能获取写锁（只能存在一个写者或多个读者，可以同时读）
>
> 使用场景：
>
> **读**多于**写**的情况（既保证线程安全，又保证性能不太差）
>
> 底层实现：
>
> 互斥锁对应的是底层结构是sync.RWMutex结构体，位于 src/sync/rwmutex.go中：
>
> ```go
> type RWMutex struct {
>     w           Mutex  // 复用互斥锁
>     writerSem   uint32 // 信号量，用于写等待读
>     readerSem   uint32 // 信号量，用于读等待写
>     readerCount int32  // 当前执行读的 goroutine 数量
>     readerWait  int32  // 被阻塞的准备读的 goroutine 的数量
> }
> ```
>
> 操作：
>
> 读锁的加锁与释放
>
> ```go
> func (rw *RWMutex) RLock() // 加读锁
> func (rw *RWMutex) RUnlock() // 释放读锁
> ```
>
> 加读锁：`atomic.AddInt32(&rw.readerCount, 1)` 调用这个原子方法，对当前在读的数量加1，如果返回负数，那么说明当前有其他写锁，这时候就调用 `runtime_SemacquireMutex` 休眠当前goroutine 等待被唤醒。
>
> 释放读锁：解锁的时候对正在读的操作减1，如果返回值小于 0 那么说明当前有在写的操作，这个时候调用 `rUnlockSlow` 进入慢速通道。被阻塞的准备读的 goroutine 的数量减1，readerWait 为 0，就表示当前没有正在准备读的 goroutine 这时候调用 `runtime_Semrelease` 唤醒写操作。
>
> 写锁的加锁与释放
>
> ```go
> func (rw *RWMutex) Lock() // 加写锁
> func (rw *RWMutex) Unlock() // 释放写锁
> ```
>
> 加写锁：首先调用互斥锁的 lock，获取到互斥锁之后，如果计算之后当前仍然有其他 goroutine 持有读锁，那么就调用 `runtime_SemacquireMutex` 休眠当前的 goroutine 等待所有的读操作完成。
>
> 释放写锁：解锁的操作，会先调用 `atomic.AddInt32(&rw.readerCount, rwmutexMaxReaders)` 将恢复之前写入的负数，然后根据当前有多少个读操作在等待，循环唤醒。
>
> **互斥锁和读写锁区别：**
>
> - 读写锁区分读者和写者，而互斥锁不区分
> - 互斥锁同一时间只允许一个线程访问该对象，无论读写；读写锁同一时间内只允许一个写者，但是允许多个读者同时读对象。

- 5.Go 可重入锁如何实现？

> 可重入锁又称为递归锁，是指在同一个线程在外层方法获取锁的时候，在进入该线程的内层方法时会自动获取锁，不会因为之前已经获取过还没释放再次加锁导致死锁。
>
> Go语言中没有可重入锁的。要实现一个可重入锁需要这两点：
>
> - 记住持有锁的线程
> - 统计重入的次数
>
> [代码实现](https://gitlab.com/893376179/golang-basic-review/-/blob/main/mutex/reentrant_Mutex/reentrant_mutex.go)

- 6.Go 原子操作有哪些？

> Go atomic包是最轻量级的锁（也称无锁结构），可以在不形成临界区和创建互斥量的情况下完成并发安全的值替换操作，不过这个包只支持int32/int64/uint32/uint64/uintptr这几种数据类型的一些基础操作（增减、交换、载入、存储等）
>
> 原子操作仅会由一个独立的CPU指令代表和完成。原子操作是无锁的，常常直接通过CPU指令直接实现。
>
> 使用场景：
>
> 当我们想要对**某个变量**并发安全的修改，除了使用官方提供的 `mutex`，还可以使用 sync/atomic 包的原子操作，它能够保证对变量的读取或修改期间不被其他的协程所影响。
>
> atomic 包提供的原子操作能够确保任一时刻只有一个goroutine对变量进行操作，善用 atomic 能够避免程序中出现大量的锁操作。
>
> 常见操作：
>
> - 增减Add
> - 载入Load
> - 比较并交换CompareAndSwap
> - 交换Swap
> - 存储Store
>
> atomic 操作的对象是一个地址，你需要把可寻址的变量的地址作为参数传递给方法，而不是把变量的值传递给方法。
>
> [代码实现](https://gitlab.com/893376179/golang-basic-review/-/blob/main/mutex/atomic_basement/atomic_basement.md)

- 7.Go 原子操作和锁的区别？

> - 原子操作由底层硬件支持，而锁是基于原子操作+信号量完成的。若实现相同的功能，前者通常会更有效率
> - 原子操作是单个指令的互斥操作；互斥锁/读写锁是一种数据结构，可以完成临界区（多个指令）的互斥操作，扩大原子操作的范围
> - 原子操作是无锁操作，属于乐观锁；说起锁的时候，一般属于悲观锁
> - 原子操作存在于各个指令/语言层级，比如“机器指令层级的原子操作”，“汇编指令层级的原子操作”，“Go语言层级的原子操作”等。
> - 锁也存在于各个指令/语言层级中，比如“机器指令层级的锁”，“汇编指令层级的锁”，“Go语言层级的锁”等

### Goroutine

- 1.Go goroutine的底层实现原理？

> Goroutine可以理解为一种Go语言的协程（轻量级线程），是Go支持高并发的基础，属于用户态的线程，由Go runtime管理而不是操作系统。
>
> **创建**：通过`go`关键字调用底层函数`runtime.newproc()`创建一个`goroutine`
>
> 当调用该函数之后，goroutine会被设置成`runnable`状态
>
> ```go
> func main() {
>    go func() {
>     fmt.Println("func routine")
>    }()
>    fmt.Println("main goroutine")
> }
> ```
>
> 创建好的这个goroutine会新建一个自己的栈空间，同时在G的sched中维护栈地址与程序计数器这些信息。
>
> 每个 G 在被创建之后，都会被优先放入到本地队列中，如果本地队列已经满了，就会被放入到全局队列中。
>
> **运行**：goroutine 本身只是一个数据结构，真正让 goroutine 运行起来的是**调度器**。Go 实现了一个用户态的调度器（GMP模型），这个调度器充分利用现代计算机的多核特性，同时让多个 goroutine 运行，同时 goroutine 设计的很轻量级，调度和上下文切换的代价都比较小。
>
> <img src="https://mmbiz.qpic.cn/mmbiz_png/DgiaeKic2zwR7MeATLaFv7D0UpgJlqnU39USZHN4OXEITKDP4lS0ycloX9Posac3NB1yZEN131DSictJXO8k3WSrQ/640?wx_fmt=png&wxfrom=5&wx_lazy=1&wx_co=1" alt="图片" style="zoom: 50%;" />
>
> **调度时机：**
>
> - 新起一个协程和协程执行完毕
> - 会阻塞的系统调用，比如文件io、网络io
> - channel、mutex等阻塞操作
> - time.sleep
> - 垃圾回收之后
> - 主动调用runtime.Gosched()
> - 运行过久或系统调用过久等等
>
> 每个 M 开始执行 P 的本地队列中的 G时，goroutine会被设置成`running`状态。
>
> 如果某个 M 把本地队列中的G都执行完成之后，然后就会去全局队列中拿 G，这里需要注意，每次去全局队列拿 G 的时候，都需要上锁，避免同样的任务被多次拿。
>
> 如果全局队列都被拿完了，而当前 M 也没有更多的 G 可以执行的时候，它就会去其他 P 的本地队列中拿任务，这个机制被称之为 work stealing 机制，每次会拿走一半的任务，向下取整，比如另一个 P 中有 3 个任务，那一半就是一个任务。
>
> 当全局队列为空，M 也没办法从其他的 P 中拿任务的时候，就会让自身进入自旋状态，等待有新的 G 进来。最多只会有 GOMAXPROCS 个 M 在自旋状态，过多 M 的自旋会浪费 CPU 资源。
>
> **阻塞：**
>
> channel的读写操作、等待锁、等待网络数据、系统调用等都有可能发生阻塞，会调用底层函数`runtime.gopark()`，会让出CPU时间片，让调度器安排其它等待的任务运行，并在下次某个时候从该位置恢复执行。
>
> 当调用该函数之后，goroutine会被设置成`waiting`状态。
>
> **唤醒：**
>
> 处于waiting状态的goroutine，在调用`runtime.goready()`函数之后会被唤醒，唤醒的goroutine会被重新放到M对应的上下文P对应的runqueue中，等待被调度。
>
> 当调用该函数之后，goroutine会被设置成`runnable`状态。
>
> **退出：**
>
> 当goroutine执行完成后，会调用底层函数`runtime.Goexit()`
>
> 当调用该函数之后，goroutine会被设置成`dead`状态。
>
> [为什么Go的协程调度很快？](https://mp.weixin.qq.com/s?__biz=MzU3OTg2NDE0Mw==&mid=2247484697&idx=1&sn=ce562f535f5e9a4de5080878393f5e72&chksm=fd5ed639ca295f2f8268b6f5a7debca84d435f3fa226b177b455584d9a56b4f2d4a2902d6c7e&token=2137302191&lang=zh_CN#rd)

- 2.Go goroutine和线程的区别?


|            | goroutine                                                    | 线程                                                         |
| ---------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 内存占用   | 创建一个 goroutine 的栈内存消耗为 2 KB。实际运行过程中，如果栈空间不够用，会自动进行扩容。 | 创建一个线程的栈内存消耗为 1 MB。                            |
| 创建和销毁 | goroutine 因为是由 Go runtime 负责管理的，创建和销毁的消耗非常小，是用户级。 | 线程创建和销毀都会有巨大的消耗，因为要和操作系统打交道，是内核级的，通常解决的办法就是线程池。 |
| 切换       | goroutines 切换只需保存三个寄存器：PC、SP、BP。goroutine 的切换约为 200 ns，相当于 2400-3600 条指令。 | 当线程切换时，需要保存各种寄存器，以便恢复现场。线程切换会消耗 1000-1500 ns，相当于 12000-18000 条指令。 |

- 3.Go goroutine泄露的场景?

> 泄漏原因：
>
> - Goroutine 内进行channel/mutex 等读写操作被一直阻塞。
> - Goroutine 内的业务逻辑进入死循环，资源一直无法释放。
> - Goroutine 内的业务逻辑进入长时间等待，有不断新增的 Goroutine 进入等待
>
> 泄漏场景：
>
> - nil channel
> - 发送不接收
> - 接收不发送
> - http request body未关闭
> - 互斥锁忘记解锁
> - sync.WaitGroup使用不当
>
> [代码实现](https://gitlab.com/893376179/golang-basic-review/-/blob/main/goroutine/goroutine_leak/goroutine_leak.go)
>
> **如何排查**：
>
> 单个函数：调用 `runtime.NumGoroutine` 方法来打印 执行代码前后Goroutine 的运行数量，进行前后比较，就能知道有没有泄露了。
>
> 生产/测试环境：使用`PProf`实时监测Goroutine的数量。

- 4.Go 如何查看正在执行的goroutine数量?

> 在程序中引入pprof package
>
> ```go
> import _ "net/http/pprof"
> ```
>
> 在命令行下执行：
>
> ```bash
> go tool pprof -http=:1248 http://127.0.0.1:6060
> ```
>
> 打开浏览器可以清晰的看到goroutine的数量以及调用关系。
>
> [示例代码](https://gitlab.com/893376179/golang-basic-review/-/blob/main/goroutine/pprof/pprof.go)

- 5.Go 如何控制并发的goroutine数量?

> **有缓冲channel**: 利用缓冲满时发送阻塞的特性。
>
> **无缓冲channel**: 任务发送和执行分离，指定消费者并发协程数。
>
> [代码实现](https://gitlab.com/893376179/golang-basic-review/-/tree/main/goroutine/control_concurrency)

### 内存管理

> [!NOTE]
>
> 这部分推荐幼麟实验室Golang合辑内存管理部分：
>
> 堆内存分配：mallocgc函数介绍
>
> <p align="center">
>
> <iframe src="//player.bilibili.com/player.html?aid=541205956&bvid=BV1gT4y1o7H1&cid=208099724&page=1&as_wide=1&high_quality=1&danmaku=1" allowfullscreen="true" width="100%" height="450" scrolling="no" frameborder="0" sandbox="allow-top-navigation allow-same-origin allow-forms allow-scripts"></iframe>
>
>
>  栈内存管理：
>
> <p align="center">
>
> <iframe src="//player.bilibili.com/player.html?aid=541205956&bvid=BV1qq4y1d7g6&cid=208099724&page=1&as_wide=1&high_quality=1&danmaku=1" allowfullscreen="true" width="100%" height="450" scrolling="no" frameborder="0" sandbox="allow-top-navigation allow-same-origin allow-forms allow-scripts"></iframe>
>
> 

### 并发编程

- 1.Go 常用的并发模型？

> 并发模型说的是系统中的线程如何协作完成并发任务，不同的并发模型，线程以不同的方式进行**通信**和协作。
>
> **线程间通信方式：**
>
> 线程间通信方式有两种：共享内存和消息传递，无论是哪种通信模型，线程或者协程最终都会从内存中获取数据，所以更为准确的说法是直接共享内存、发送消息的方式来同步信息。
>
> **共享内存**
>
> 抽象层级：抽象层级低，当我们遇到对资源进行更细粒度的控制或者对性能有极高要求的场景才应该考虑抽象层级更低的方法。
>
> 耦合：高，线程需要在读取或者写入数据时先获取保护该资源的互斥锁。
>
> 线程竞争：需要加锁，才能避免线程竞争和数据冲突。
>
> **消息传递**
>
> 抽象层级：抽象层级高，提供了更良好的封装和与领域更相关和契合的设计，比如Go 语言中的`Channel`就提供了 Goroutine 之间用于传递信息的方式，它在内部实现时就广泛用到了共享内存和锁，通过对两者进行的组合提供了更高级的同步机制。
>
> 耦合：低，生产消费者模型。
>
> 线程竞争：保证同一时间只有一个活跃的线程能够访问数据，channel维护所有被该chanel阻塞的协程，保证有资源的时候只唤醒一个协程，从而避免竞争。
>
> ----
>
> Go语言中实现了两种并发模型，一种是共享内存并发模型，另一种则是CSP模型。
>
> **共享内存并发模型**
>
> 通过直接共享内存 + 锁的方式同步信息，传统多线程并发。
>
> <img src="https://image-1302243118.cos.ap-beijing.myqcloud.com/img/image-20220514161804822.png" alt="img" style="zoom:67%;" />
>
> **CSP并发模型**
>
> 通过发送消息的方式来同步信息，Go语言推荐使用的*通信顺序进程*（communicating sequential processes）并发模型，通过goroutine和channel来实现
>
> - `goroutine` 是Go语言中并发的执行单位，可以理解为”线程“
> - `channel`是Go语言中各个并发结构体(`goroutine`)之前的通信机制。 通俗的讲，就是各个`goroutine`之间通信的”管道“，类似于Linux中的管道
>
> <img src="https://image-1302243118.cos.ap-beijing.myqcloud.com/img/image-20220514161841437.png" alt="img" style="zoom:67%;" />

- 2.Go 有哪些并发同步原语？

> **原子操作**：Mutex、RWMutex 等并发原语的底层实现是通过 atomic 包中的一些原子操作来实现的，原子操作是最基础的并发原语。[代码示例](https://gitlab.com/893376179/golang-basic-review/-/blob/main/concurrency/atomic/atomic.go)
>
> **Channel**：goroutine之间通信的桥梁。使用场景：消息队列、数据传递、信号通知、任务编排、锁。[代码示例](https://gitlab.com/893376179/golang-basic-review/-/blob/main/concurrency/channel/channel.go)
>
> **基本并发原语**：Go 语言在 `sync`包中提供了用于同步的一些基本原语，这些基本原语提供了较为基础的同步功能，但是它们是一种相对原始的同步机制，在多数情况下，我们都应该使用抽象层级更高的 Channel 实现同步。
>
> 常见的并发原语如下：`sync.Mutex`、`sync.RWMutex`、`sync.WaitGroup`、`sync.Cond`、`sync.Once`、`sync.Pool`、`sync.Context`
>
> `sync.Mutex` （互斥锁） 可以限制对临界资源的访问，保证只有一个 goroutine 访问共享资源
>
> 使用场景：大量读写，比如多个 goroutine 并发更新同一个资源，像计数器。[代码示例](https://gitlab.com/893376179/golang-basic-review/-/blob/main/concurrency/sync_package/mutex/mutex.go)
>
> ----
>
> `sync.RWMutex` （读写锁） 可以限制对临界资源的访问，保证只有一个 goroutine 写共享资源，可以有多个goroutine 读共享资源。
>
> 使用场景：大量并发读，少量并发写，有强烈的性能要求。[代码示例](https://gitlab.com/893376179/golang-basic-review/-/blob/main/concurrency/sync_package/rwMutex/rwMutex.go)
>
> ----
>
> `sync.WaitGroup` 可以等待一组 Goroutine 的返回。
>
> 使用场景：并发等待，任务编排，一个比较常见的使用场景是批量发出 RPC 或者 HTTP 请求。[代码示例](https://gitlab.com/893376179/golang-basic-review/-/blob/main/concurrency/sync_package/waitGroup/waitGroup.go)
>
> ----
>
> `sync.Cond` 可以让一组的 Goroutine 都在满足特定条件时被唤醒。
>
> 使用场景：利用等待 / 通知机制实现阻塞或者唤醒。[代码示例](https://gitlab.com/893376179/golang-basic-review/-/blob/main/concurrency/sync_package/cond/cond.go)
>
> ----
>
> `sync.Once` 可以保证在 Go 程序运行期间的某段代码只会执行一次。
>
> 使用场景：常常用于单例对象的初始化场景。[代码示例](https://gitlab.com/893376179/golang-basic-review/-/blob/main/concurrency/sync_package/once/once.go)
>
> ---
>
> `sync.Pool`可以将暂时将不用的对象缓存起来，待下次需要的时候直接使用，不用再次经过内存分配，复用对象的内存，减轻 GC 的压力，提升系统的性能（频繁地分配、回收内存会给 GC 带来一定的负担，严重的时候会引起 CPU 的毛刺）。
>
> 使用场景：对象池化， TCP连接池、数据库连接池、Worker Pool。[代码示例](https://gitlab.com/893376179/golang-basic-review/-/blob/main/concurrency/sync_package/pool/pool.go)
>
> ----
>
> `sync.Map` 线程安全的map。
>
> 使用场景：map 并发读写。[代码示例](https://gitlab.com/893376179/golang-basic-review/-/blob/main/concurrency/sync_package/map/map.go)
>
> ---
>
> `sync.Context` 可以进行上下文信息传递、提供超时和取消机制、控制子 goroutine 的执行。
>
> 使用场景：取消一个goroutine的执行。[代码示例](https://gitlab.com/893376179/golang-basic-review/-/blob/main/concurrency/sync_package/context/context.go)
>
> ---
>
> **扩展并发原语**
>
> `errgroup` 可以在一组 Goroutine 中提供了同步、错误传播以及上下文取消的功能。
>
> 使用场景：只要一个 goroutine 出错我们就不再等其他 goroutine 了，减少资源浪费，并且返回错误。[代码示例](https://gitlab.com/893376179/golang-basic-review/-/blob/main/concurrency/sync_package/extend_concurrency/errGroup/errGroup.go)
>
> `Semaphore`带权重的信号量，控制多个goroutine同时访问资源。
>
> 使用场景：控制 goroutine 的阻塞和唤醒。[代码示例](https://gitlab.com/893376179/golang-basic-review/-/blob/main/concurrency/sync_package/extend_concurrency/semaphore/semaphore.go)
>
> `SingleFlight`用于抑制对下游的重复请求。
>
> 使用场景：访问缓存、数据库等场景，缓存过期时只有一个请求去更新数据库。[示例代码](https://gitlab.com/893376179/golang-basic-review/-/blob/main/concurrency/sync_package/extend_concurrency/singleFlight/singleFlight.go)

- 3.Go WaitGroup实现原理？

> `Go`标准库提供了`WaitGroup`原语, 可以用它来等待一批 Goroutine 结束。
>
> **底层数据结构**
>
> ```go
> type WaitGroup struct {
>  noCopy noCopy
>  state1 [3]uint32
> }
> ```
>
> 其中 `noCopy` 是 golang 源码中检测禁止拷贝的技术。如果程序中有 WaitGroup 的赋值行为，使用 `go vet` 检查程序时，就会发现有报错。但需要注意的是，noCopy 不会影响程序正常的编译和运行。
>
> `state1`主要是存储着状态和信号量，状态维护了 2 个计数器，一个是请求计数器counter ，另外一个是等待计数器waiter（已调用 `WaitGroup.Wait` 的 goroutine 的个数）
>
> 当数组的首地址是处于一个`8`字节对齐的位置上时，那么就将这个数组的前`8`个字节作为`64`位值使用表示状态，后`4`个字节作为`32`位值表示信号量(`semaphore`)；同理如果首地址没有处于`8`字节对齐的位置上时，那么就将前`4`个字节作为`semaphore`，后`8`个字节作为`64`位数值。
>
> <img src="https://image-1302243118.cos.ap-beijing.myqcloud.com/img/image-20220522104433409.png" alt="img" style="zoom:67%;" />
>
> **使用方法**
>
> 在WaitGroup里主要有3个方法：
>
> - `WaitGroup.Add()`：可以添加或减少请求的goroutine数量，*`Add(n)` 将会导致 `counter += n`*。
> - `WaitGroup.Done()`：相当于Add(-1)，`Done()` 将导致 `counter -=1`，请求计数器counter为0 时通过信号量调用`runtime_Semrelease`唤醒waiter线程。
> - `WaitGroup.Wait()`：会将 `waiter++`，同时通过信号量调用 `runtime_Semacquire(semap)`阻塞当前 goroutine。

- 4.Go Cond实现原理？

> `Go`标准库提供了`Cond`原语，可以让 Goroutine 在满足特定条件时被阻塞和唤醒。
>
> **底层数据结构**
>
> ```go
> type Cond struct {
>     noCopy noCopy
> 
>     // L is held while observing or changing the condition
>     L Locker
> 
>     notify  notifyList
>     checker copyChecker
> }
> 
> type notifyList struct {
>     wait   uint32
>     notify uint32
>     lock   uintptr // key field of the mutex
>     head   unsafe.Pointer
>     tail   unsafe.Pointer
> }
> ```
>
> 主要有`4`个字段：
>
> - `nocopy` ： golang 源码中检测禁止拷贝的技术。如果程序中有 WaitGroup 的赋值行为，使用 `go vet` 检查程序时，就会发现有报错，但需要注意的是，noCopy 不会影响程序正常的编译和运行。
> - `checker`：用于禁止运行期间发生拷贝，双重检查(`Double check`)。
> - `L`：可以传入一个读写锁或互斥锁，当修改条件或者调用`Wait`方法时需要加锁。
> - `notify`：通知链表，调用`Wait()`方法的`Goroutine`会放到这个链表中，从这里获取需被唤醒的Goroutine列表。
>
> **使用方法**
>
> 在Cond里主要有3个方法：
>
> - `sync.NewCond(l Locker)`: 新建一个 sync.Cond 变量，注意该函数需要一个 Locker 作为必填参数，这是因为在 `cond.Wait()` 中底层会涉及到 Locker 的锁操作。
> - `Cond.Wait()`: 阻塞等待被唤醒，调用Wait函数前**需要先加锁**；并且由于Wait函数被唤醒时存在虚假唤醒等情况，导致唤醒后发现，条件依旧不成立，因此需要使用 for 语句来循环地进行等待，直到条件成立为止。
> - `Cond.Signal()`: 只唤醒一个最先 Wait 的 goroutine，可以不用加锁。
> - `Cond.Broadcast()`: 唤醒所有Wait的goroutine，可以不用加锁。

- 5.Go 有哪些方式安全读写共享变量？

| **方法**                                                     | **并发原语**                       | **备注**                                            |
| ------------------------------------------------------------ | ---------------------------------- | --------------------------------------------------- |
| 不要修改变量                                                 | sync.Once                          | 不要去写变量，变量只初始化一次                      |
| 只允许一个goroutine访问变量                                  | Channel                            | 不要通过共享变量来通信，通过通信(channel)来共享变量 |
| 允许多个goroutine访问变量，但是同一时间只允许一个goroutine访问 | sync.Mutex、sync.RWMutex、原子操作 | 实现锁机制，同时只有一个线程能拿到锁                |

- 6.Go 如何排查数据竞争问题？

> 首先只要有两个以上的goroutine并发访问同一变量，且至少其中的一个是写操作的时候就会发生数据竞争；全是读的情况下是不存在数据竞争的。
>
> **排查方式**
>
> ```go
> package main
> 
> import "fmt"
> 
> func main() {
>     i := 0
> 
>     go func() {
>         i++ // write i
>     }()
> 
>     fmt.Println(i) // read i
> }
> ```
>
> `go命令行`有个参数`race`可以帮助检测代码中的数据竞争。
>
> ```bash
> $ go run -race main.go
> 
> WARNING: DATA RACE
> Write at 0x00c0000ba008 by goroutine 7:
> exit status 66
> ```

### 调试工具

> [!NOTE]
>
> Go语言原生支持对于程序运行时重要指标和特征的分析，以及事件的追踪。
>
> pprof工具可实现对于指标和特征的分析，通过分析可以查找到程序中的错误，比如内存泄露，race冲突，协程泄漏，cpu利用率过高等；也可以对程序进行优化。
>
> trace工具是以事件为基础的信息追踪，可反映一段时间内程序的变化，比如频繁的gc及协程调度等。
>
> 在生产环境中，由于go语言运行时的指标不对外暴露，需要通过一系列工具分析查询。而且在很多情形下，我们往往也不会直接通过http接口调用的方式在生产环境中直接分析特征文件，需要将特征文件取样下载到本地之后再解析分析。
>
> [Golang调试工具pprof与trace](https://mp.weixin.qq.com/s/SsCPvePTvjxzLqudZaN4Sw)



### 项目实践

> - [Golang做一个IM即时通信系统](https://mp.weixin.qq.com/s?__biz=MzU3OTg2NDE0Mw==&mid=2247484683&idx=1&sn=880d25c39f22d442ceafdf84c00d1065&chksm=fd5ed62bca295f3dbe2e4f77611849944367eb136b33350a7a02c7e3c25f051ea7fccfa65774&token=2137302191&lang=zh_CN#rd)
> - 待更新……

### 常用开源库
> - [解析Golang常用定时任务库gron和cron](https://mp.weixin.qq.com/s/T-v0J3erGulZa1EKm685Bg)
> - [Golang中用幂等思路解决缓存击穿的方案：singleflight](https://mp.weixin.qq.com/s/NG7eRfgxwAkz0zMVVqB4Tw)
> - 待更新……

### 工具及配置
> - [Go 开发人员最佳 VSCode 插件列表](https://mp.weixin.qq.com/s/cEO6ZBH96ZxbScPxHD0vug)
> - 待更新……



## Pythoner之路

---
> [!NOTE]
> - [**【python核心知识汇总（精编版）】阿巩爆肝整理，便于快速了解Python语言**](https://mp.weixin.qq.com/s?__biz=MzU3OTg2NDE0Mw==&mid=2247483850&idx=1&sn=c24b4c369e495f5a76d7516eac3fe4b9&chksm=fd5ed2eaca295bfc7e7e5303d883ad71740e0354483acae9501685d67f5afa82c22b73929365&token=2137302191&lang=zh_CN#rd)
> - [yield关键字详解](https://mp.weixin.qq.com/s?__biz=MzU3OTg2NDE0Mw==&mid=2247484056&idx=1&sn=084277edc473194ee07ba0d6d0eff676&chksm=fd5ed1b8ca2958aeaed7d989458a07378353867753a30122f401491fe1ae191f45dfb16118de&token=2137302191&lang=zh_CN#rd)
> - [Python分布式任务队列Celery，Django中如何实现异步任务和定时任务](https://mp.weixin.qq.com/s?__biz=MzU3OTg2NDE0Mw==&mid=2247483733&idx=1&sn=e39f112db45d4626deca2db0ceb9db97&chksm=fd5ed275ca295b63201776850f57fb763a4b7665777389ba4783081954da5afc3e84130d0068&token=2137302191&lang=zh_CN#rd)
> - [Django Rest framework](https://mp.weixin.qq.com/s?__biz=MzU3OTg2NDE0Mw==&mid=2247483789&idx=1&sn=9f2e2af136144b4fe2509908fd25ddba&chksm=fd5ed2adca295bbbc9a00fb17c5f7f7bedbf0861d4ac9e15f91e01d53c15f975074b65d89e8e&token=2137302191&lang=zh_CN#rd)
> - [Twisted：基于事件驱动的Python网络框架](https://mp.weixin.qq.com/s?__biz=MzU3OTg2NDE0Mw==&mid=2247484107&idx=1&sn=c13d7dfc6cebe11c1797b156ee72bff5&chksm=fd5ed1ebca2958fdf87b686c8ff8eceb68ae8b985f468fc911e2f5dc1819864cf1d936debe7b&token=2137302191&lang=zh_CN#rd)
> - 待更新……

# **常用编程软件和工具**
---
> [!NOTE]
>
> **编程软件和工具的资源部分直接放上CodeSheep羊哥整理好的手册链接，手册很全面，值得收藏！**
>
>  - [**【置顶笔记下载1】熬10天夜，我肝出了这个PDF版「服务器软件安装手册」**](https://mp.weixin.qq.com/s/YNOZSQ5smo1uGE5ZttUZ_w)
>
>  - [**【置顶笔记下载2】再肝两夜，写了个「服务器项目部署」实战PDF手册**](https://mp.weixin.qq.com/s/JUJ6qu_ec3s1JmTxQt_V3g)

> **工欲善其事，必先利其器！**

一个易上手、好用、高效的软件工具对于程序员工作效率的提升是不言而喻的，这些常用开发软件和工具比如：

> - 基础的软件开发环境和设施
> - Linux操作系统
> - 版本控制工具
> - 高效的IDE和编辑器
> - API管理工具/文档管理工具
> - CI守护系统
> - ...... 等等

接下来我们一项一项详细盘点。

## 基础软件开发环境和设施

---
> [!Note]
>  **什么叫基础软件开发环境？**
>  
>  很好理解。学Java得要装JDK吧？学Python得要Python环境吧？学数据库得要MySQL吧？等等，这些在学习编程之前都是应该提前在自己的电脑上准备好的，而且尽量把全套都先备好，后面直接就用了。

每次都是照着网上现搜的帖子操作，一顿操作猛如虎，一看结果各种有问题，要不报错，要不环境起不来，等等。装个环境就浪费了很多时间，而且过了一段时间又忘，还得重新搜帖子，该踩的坑一个都不少...

内容包含常见基础开发环境和设施的安装部署，包括：

> - Linux操作系统
> - Git工具
> - Java环境JDK
> - Node基础环境
> - Python基础环境
> - Maven项目构建和管理
> - MySQL数据库
> - Redis缓存
> - RabbitMQ消息队列
> - Tomcat应用服务器
> - Nginx Web服务器
> - Docker环境
> - Kubernetes环境
> - Elasticsearch搜索引擎
> - ZooKeeer环境
> - Kafka消息队列

大家有需要的可以：[点击此处 → 下载获取](https://mp.weixin.qq.com/s/YNOZSQ5smo1uGE5ZttUZ_w)！

---
## Linux操作系统
---

> [!ATTENTION]
>  实际企业级开发和项目部署，大部分情况下基本都是基于Linux环境进行的，所以掌握常用的命令、配置、网络和系统管理、基本的Shell编程等尽量还是要熟练一些，对后续项目实践都大有裨益。

> [!Note]
>  - [**本章节置顶资源1：《Linux命令速查备忘手册.pdf》下载**](https://mp.weixin.qq.com/s/GJYVt376C_g4406ux5uw0Q)
>  - [**本章节置顶资源2：《Linux命令行大全.pdf》下载**](https://mp.weixin.qq.com/s/3rV556_0piRNsSpDYwuZ8g)

- [常用Linux操作系统大盘点](https://mp.weixin.qq.com/s/x8rwsIOzYEPXmEVXmWJ4Tg)
- [人手一套Linux环境之：macOS版教程](https://mp.weixin.qq.com/s/WeZLtfrMdnISpX3v5WpJfA)
- [人手一套Linux环境之：Windows版教程](https://mp.weixin.qq.com/s/onVwwEQ1DAwbvK7qS2YNxg)
- [69张图：详细记录Ubuntu 20.04安装配置过程](https://mp.weixin.qq.com/s/vkLZ_3Jp4HdQ8PDIMYsGEw)
- [废柴电脑拯救计划：借助Debian搭建个人专属云服务器](https://mp.weixin.qq.com/s/YpWp-b3vcAtb_jBwCM7wtg)
- [常用Linux命令大整理](https://mp.weixin.qq.com/s/Kog6AfXYINIDwKMpJMkGFQ)
- [面试常问的20个Linux命令](https://mp.weixin.qq.com/s/3NI4FWuOfYMJBiKqqnpqMA)
- [Linux 桌面进化史](https://mp.weixin.qq.com/s/jQAJQo28UsY3YIxC5ci1sA)
- [Linux迎来29岁：从个人爱好到统治世界的操作系统内核！](https://mp.weixin.qq.com/s/6-yb1N-SwHr4EaQ-fvU4Ow)
- [漫画：Linux 内核到底长啥样？](https://mp.weixin.qq.com/s/4G6re30hxAacxmCLjP8KpQ)
- [资源下载 → 《Linux命令速查备忘手册.pdf》](https://mp.weixin.qq.com/s/GJYVt376C_g4406ux5uw0Q)
- [资源下载 → 《Linux命令行大全.pdf》](https://mp.weixin.qq.com/s/3rV556_0piRNsSpDYwuZ8g)

---
## 版本控制工具
---

> - [资源下载 → 豆瓣9.1分的Pro Git学习手册YYDS！](https://mp.weixin.qq.com/s/xDYSG4uDz9rgNCuk0RxLjg)
> - [科普篇：Git和SVN对比](https://mp.weixin.qq.com/s/bUYz9JwqAYH_Fn6nHPSmTg)
> - [科普篇：5分钟了解Git的前世今生](https://mp.weixin.qq.com/s/VvpjFUXd6jcatACHyFPHfg)
> - [科普篇：Git操作与常用命令集锦](https://mp.weixin.qq.com/s/swnwBiuyVmhs5iPqv3H6BQ)
> - [原理篇：图解Git中的最常用命令](https://mp.weixin.qq.com/s/DRCeDhYiwQQToKukk4RM4g)
> - [原理篇：从原理角度理解记忆Git常见命令](https://mp.weixin.qq.com/s/DQVVYOWdOPuRsy3m0fg6Xg)
> - [实践篇：从实践中彻底上手Git](https://mp.weixin.qq.com/s/sp1YUQ2vnQaIGH4tO3j1Vw)
> - [实践篇：Git分支开发](https://mp.weixin.qq.com/s/touo-rygtz0tG6y8NKw65A)
> - [实践篇：如何使用GitHub Flow给开源项目贡献代码](https://mp.weixin.qq.com/s/JMNQi3BSTmKpF9vXMEdKHw)
> - [技巧篇：一招搞定GitHub下载加速！](https://mp.weixin.qq.com/s/SYR4zvjhAH1mX9fxdp8cbA)
> - [技巧篇：如何在同一台电脑上同时使用多个Git账号](https://mp.weixin.qq.com/s/Qt4bqReZU3ydIsMtCVC0eA)

---
## IDE/编辑器

> - [IDEA 2021.2升级体验](https://mp.weixin.qq.com/s/WQiHpAv4bjEIz3fghzamEA)
> - [IDEA 2021.1升级体验](https://mp.weixin.qq.com/s/dojfVvkrxru1eH8Pep7irw)
> - [IDEA 2020.3升级体验](https://mp.weixin.qq.com/s/FhURpMKa9oi2FrV_0woOtw)
> - [IDEA 2020.1升级体验](https://mp.weixin.qq.com/s/Ua4TYlcNntLr-x9WQirHyg)
> - [IDEA插件分享：开发必备的IDEA神级插件大分享](https://mp.weixin.qq.com/s/xMwGAL_7sGkmnFvWZroVPw)
> - [我的IDEA写代码小技巧：幸福感+效率爆棚](https://mp.weixin.qq.com/s/V2uAxmdq2e0Hl-y5GaD-Qw)
> - [为啥我的IDEA Maven依赖下载总是失败？](https://mp.weixin.qq.com/s/KNk04dv6Z-ERrHiY03qdAw)
> - [试水JetBrains官方新编程字体](https://mp.weixin.qq.com/s/RK_ygHgMjayL5-qTnlHbeg)
> - [如何使用IDEA远程调试线上代码](https://mp.weixin.qq.com/s/WLTfgkPnJYJf9PZM8d8w4Q)
> - [利用VS Code进行远程开发教程](https://mp.weixin.qq.com/s/ZDVYk188oPLugRI6oeVYIQ)
> - [VS Code常用插件+快捷键整理](https://mp.weixin.qq.com/s/_3mwj5_MNln__3SSI8BJ3Q)

---
## API管理/文档管理
---

> - [科普篇：前后端都分离了，有哪些好用的API管理系统？](https://mp.weixin.qq.com/s/Ahs6fnIfFVVPOn3NZpIsNA)
> - [科普篇：几款常用的在线文档神器](https://mp.weixin.qq.com/s/G6-6gqYnTvEsWOGIoj16ZQ)
> - [一款零注解API接口文档生成工具](https://mp.weixin.qq.com/s/I_pH1V9iUu-IUayMA5oQSg)

---
## CI系统

> - [常用CI工具大盘点](https://mp.weixin.qq.com/s/PgoXJcJX_sm5132VGQoZ1w)
> - [Jenkins Pipeline使用入门](https://mp.weixin.qq.com/s/Y2gxBmBK2HPvr1uKUUxRYA)
> - [讨论：你们公司用啥CI守护系统？](https://mp.weixin.qq.com/s/4NlLEyy2QXX5_snX4r7FSw)
> - [GitLab+Docker快速搭建CI/CD自动化部署](https://mp.weixin.qq.com/s/GHGaIfCVakuE1PX_Qzc9IA)
> - [GitOps 应用实践系列 - Argo CD 的基本介绍](https://mp.weixin.qq.com/s/fIEsYHt0yqygY4b3AmM1Qw)

## 其他软件/工具/网站

> - [自学数据结构和算法的9大工具和网站](https://mp.weixin.qq.com/s/f9dfQQbpKjMopH6m2Gjiiw)
> - [12个常见的编程开发自学网站汇总](https://mp.weixin.qq.com/s/jFc-6QK2Mv1zHuhuFrqhog)
> - [常用Web服务器软件大盘点](https://mp.weixin.qq.com/s/J1XjIwtEKjaltqWH-0qmgw)
> - [9款最佳编程字体推荐](https://mp.weixin.qq.com/s/VB1jGIWWp4XdsLwXe-PKlQ)
> - [5款时间管理工具推荐](https://mp.weixin.qq.com/s/71Cr9UxvQc6CdmguQIh-IA)
> - [一个程序员的常用在线工具网站分享](https://mp.weixin.qq.com/s/oMAJaKDfr_bmgCVD0CX1oA)
> - [2020年度开发者工具Top 100名单](https://mp.weixin.qq.com/s/VtmcuYGt96S4750UFYKnXQ)
> - [Linux服务器上几个常用的监控小工具整理](https://mp.weixin.qq.com/s/zWIv5yBTD0Tvt8txb8znrA)
> - [效率神器PowerToys使用攻略](https://mp.weixin.qq.com/s/LUPa_uSEd91Pj08VwxsxEQ)
> - [22款适合程序员的终端生产力小工具推荐](https://mp.weixin.qq.com/s/k19ZT_yH4lzNLUYkH2GUJA)
> - [9款优秀的代码比对工具推荐](https://mp.weixin.qq.com/s/IU5mmLxflgswICEo3BCWLA)
> - [几款提升效率的软件神器](https://mp.weixin.qq.com/s/HZRz_A8bLcOTuzcezYEFZg)
> - [命令行效率神器 NuShell ！](https://mp.weixin.qq.com/s/0f6JpBiPqCY8cZ2Ich0Lmg)

---
---
# **计算机基础**

---
> [!ATTENTION]
> **计算机基础是最最重要的！**即使后面的东西都没学（或者来不及学），基础部分是肯定需要完成的，这也是后面所有应用框架学习和做项目的基石；反之，在应用框架的学习或者做项目时如果感觉吃力，那就可能非常有必要回过头再来巩固对应的基础知识。
> 计算机基础的学习不是一蹴而就的，需要一定的反复和回炉才能做到真正的融会贯通，需要一个过程。珍惜每一次回顾和复习基础的机会，应扎实精进，步步为营。
> 那我们口口声声所说的编程基础（计算机基础）到底指是什么呢？
> 此处整理为**六大方面：**
>
>  - 一两门你最熟悉的编程语言（不求多，但求精！）
>  - 数据结构和算法
>  - 计算机网络
>  - 操作系统（计组）
>  - 数据库/SQL
>  - 设计模式
---

## 数据结构+算法题

---
> [!WARNING]
>  数据结构和算法题对于程序员的重要性不言而喻。参加过笔试或者面试过的小伙伴应该知道，可以说这个东西某一程度上直接决定了面试的成败，现在的互联网公司技术岗面试，数据结构+算法题基本都是必选项。除了几种基础数据结构类型得烂熟于心，基本的几大算法(或者说算法思想)也要了如指掌之外，参加求职前，不论是校招还是社招，非常有必要好好刷一刷LeetCode上的数据结构和算法题，以保持题感。
---

- [资源下载 → LeetCode算法刷题Golang版答案PDF文档](https://mp.weixin.qq.com/s/EaDLaLy3YjrNiSoNofwMMA)
- [资源下载 → LeetCode算法刷题C++版答案PDF文档](https://mp.weixin.qq.com/s/qR6KTfldk41lsOj1Ghp6Og)
- [资源下载 → LeetCode算法刷题Java版答案PDF文档](https://mp.weixin.qq.com/s/kWTJ9640mPegr5wqVE0GMg)
- [资源下载 → 谷歌学长的数据结构+算法题代码笔记手册](https://mp.weixin.qq.com/s/gXH98f1p5cCYVETCn9w9kA)
- [数据结构：24张图详解九大数据结构](https://mp.weixin.qq.com/s/ZVwIUN-xf9FuxOFXW8H3Nw)
- [数据结构：哈希表详解](https://mp.weixin.qq.com/s/UanDueZi3MwlcKYGMNQPGg)
- [数据结构：栈Stack的几种含义理解](https://mp.weixin.qq.com/s/v6rr0M9AW_kSMEtjkc-Mqw)
- [数据结构：各种树详解](https://mp.weixin.qq.com/s/k4-RaW4ROlo6chSXsO_4AA)
- [数据结构：跳表精讲](https://mp.weixin.qq.com/s/czkZcQL8mEqG2xeX8huqsA)
- [数据结构：数组和链表的性能对比](https://mp.weixin.qq.com/s/iG9zNHYLkEyHF3M4RpoWKw)
- [算法：十大经典排序算法大梳理 (动图+代码)](https://mp.weixin.qq.com/s/ekGdneZrMa23ALxt5mvKpQ)
- [算法：排序算法趣味对比](https://mp.weixin.qq.com/s/za_MJY3-r9Gfiu3BJfej7Q)
- [算法：二分法及其变种](https://mp.weixin.qq.com/s/1ExIav9uK4bvVnnf4t0H2Q)
- [算法：10张图搞定KMP算法](https://mp.weixin.qq.com/s/H1ttBFuBY5n3Dbh3YaTmlw)
- [算法：面试官最爱的字符串匹配算法精讲](https://mp.weixin.qq.com/s/FYx3acRh9JXAIb7y97G39A)
- [算法：8大常见算法思想总结](https://mp.weixin.qq.com/s/vGjcX4nHv6QT2-qKHZM8Dw)
- [算法：一文彻底学会递归思路解题](https://mp.weixin.qq.com/s/JLbjzCdVlJ_de2uGgBsUzw)
- [算法：10张动图理解递归](https://mp.weixin.qq.com/s/uuNaZfzhG3KaoEtez8Migw)
- [算法：分治算法详解](https://mp.weixin.qq.com/s/a3_bMRmTqZxMBruYsPC9-w)
- [算法：递归算法详解](https://mp.weixin.qq.com/s/tqGKHZzSyDBgEp-oWsOztQ)
- [算法：动态规划常见算法题例析](https://mp.weixin.qq.com/s/bGH_o1BsJtgnC2rFugtsRw)
- [算法：搞定算法复杂度分析](https://mp.weixin.qq.com/s/mBTyBkeNHoW7-Rcbv2Exwg)
- [算法：常用算法复杂度速查表](https://mp.weixin.qq.com/s/U6D1PNjuBAcRd5UZRr0F3w)
- [算法：常见刷题模式套路分析](https://mp.weixin.qq.com/s/JhHPAaOInIi4lZ-UrCLdgg)
- [算法：算法题刷题的心得和建议](https://mp.weixin.qq.com/s/Gz9-f_G6P-0tKrReAinw0A)
- [算法：求职必刷算法题大集锦](https://mp.weixin.qq.com/s/HUS12phrTbNzAE6uFHO9lg)
- [工具推荐：自学数据结构和算法的9大工具和网站](https://mp.weixin.qq.com/s/f9dfQQbpKjMopH6m2Gjiiw)

---

## 计算机网络

---
> [!WARNING]
>  为什么一定要学好计算机网络？原因很简单，因为计算机网络中的各种协议栈是当下繁荣的互联网通信的基石，尤其建议要牢固熟练地掌握TCP/IP协议栈。
---

- [视频科普：计算机网络该怎么学](https://www.bilibili.com/video/BV1U4411M71p)
- [概念科普：如何系统地学习计算机网络](https://mp.weixin.qq.com/s/v34D8xWGPW8LgENy3G2QZA)
- [概念科普：计算机网络的89个核心概念](https://mp.weixin.qq.com/s/t8Dz-4D6BuBevZYz5Jhklg)
- [工具科普：学网络时，可以先落实这几款利器工具](https://mp.weixin.qq.com/s/hZF2P4oQE6dsuXKWLEgDKA)
- [探究原理：浏览器输入网址一回车，后面到底发生了什么](https://mp.weixin.qq.com/s/t2Csrl7idUXISKW54cpV1g)
- [探究原理：你可能没有细究过的TCP/IP](https://mp.weixin.qq.com/s/AjcW7ELOTTqqdnUHpMbccQ)
- [探究原理：一个数据包在网络中到底是怎么游走的](https://mp.weixin.qq.com/s/07zloKKMUl-RHN6tWqZIJQ)
- [探究原理：两台计算机之间究竟是如何通信的](https://mp.weixin.qq.com/s/ZCddesfN0qISh3Rqo2jbWA)
- [探究原理：ping命令用得这么6，原理知道不](https://mp.weixin.qq.com/s/55bbQX2-SUNe6PEI9My5fA)
- [探究原理：一台Linux服务器最多能支撑多少个TCP连接](https://mp.weixin.qq.com/s/QuCxkSjdM_E12lXlpnhKIQ)
- [探究原理：都说HTTP协议无状态，这里的「状态」到底指什么](https://mp.weixin.qq.com/s/EZwOUGMrGKEF_POisJKmuw)
- [探究原理：Session/Cookie/Token](https://mp.weixin.qq.com/s/5oFKdbFWgZrwqESNTZn77w)
- [探究原理：TCP哪些机制保证了可靠性](https://mp.weixin.qq.com/s?__biz=MzU3OTg2NDE0Mw==&mid=2247483894&idx=1&sn=2ff12b5e7bdb7f376fd5c270a4d9aa6d&chksm=fd5ed2d6ca295bc095f170a34ed7a989cc1784288dcbd573ff3fcedabb1d7c29b85adad317d1&token=2137302191&lang=zh_CN#rd)
- [探究原理：三次握手底层深度理解](https://mp.weixin.qq.com/s/u242LSHnePBg_KbMc0ioTA)
- [知识总结：计网IP部分知识总结](https://mp.weixin.qq.com/s/21Tk-8gxpDoH9DNWNYCWmA)
- [知识总结：图解HTTP的前世今生](https://mp.weixin.qq.com/s/vk_QZDpJLVEXXNe1jYzd8w)
- [知识总结：HTTP和HTTPS协议大总结](https://mp.weixin.qq.com/s/8HJ-V1BbV9wp89HS-XulXw)
- [面试相关：GET和POST到底有什么区别](https://mp.weixin.qq.com/s/H4gbg7bfnw61jZQcapTnMw)
- [面试相关：面试最爱问的三次握手和四次挥手问题](https://mp.weixin.qq.com/s/lFnyBaaP3f0eNcKGW5RtCg)
- [面试相关：HTTP常见面试题集合](https://mp.weixin.qq.com/s/FJGKObVnU61ve_ioejLrtw)
- [面试相关：计网TCP/UDP部分高频面试题集合](https://mp.weixin.qq.com/s/doxVJZ1G6187B4AOXb0JlA)
- [面试相关：计算机网络高频12问](https://mp.weixin.qq.com/s/PIGfhMaAx9R5C4x5eJQnIw)
- [实践系列：网络排错思路大总结](https://mp.weixin.qq.com/s/L8jmABzoI_e4dCcECenHtQ)
- [实践系列：抓个包看一看浏览器里的HTTP请求到底是如何完成的](https://mp.weixin.qq.com/s/_fB7r53BGZRvpG9YAPRQ8A)

---

## 操作系统

---
> [!WARNING]
>  学好操作系统有利于我们深入理解计算机底层，这样平时在遇到疑难杂症时，能够更容易看到问题的本质，并高效解决；另外操作系统里的很多优秀的设计思想、经典的架构、算法、思路也值得我们反复理解和思考，很多思想在平时的工作中也可以借鉴和运用。
---

- [知识总结：学编程要懂的操作系统基础](https://mp.weixin.qq.com/s/ttncekujB82g88GRx3a6lQ)
- [知识总结：程序员必知的89个操作系统核心概念](https://mp.weixin.qq.com/s/VsQ7IpP-jnXSjJhOAzl-ew)
- [知识总结：操作系统主要概念硬核讲解](https://mp.weixin.qq.com/s/1Rzvu9uCTef5l_8Qw3ff0A)
- [知识总结：20张图详解操作系统内存管理部分知识点](https://mp.weixin.qq.com/s/m-AmxDVUfko7OTUsCapnPA)
- [知识总结：这才是对进程和线程最通俗易懂的解释](https://mp.weixin.qq.com/s/FoYiPB-2LuIpnTW8gck2EQ)
- [知识总结：操作系统并发三剑客之进程/线程/协程](https://mp.weixin.qq.com/s/Pfc31qoWfL6_uva6ePBmVA)
- [知识总结：操作系统高并发服务模型大科普](https://mp.weixin.qq.com/s/yXMgpAhz3JhtCtutMQnXvQ)
- [知识总结：这可能是讲死锁最通俗的一篇文章](https://mp.weixin.qq.com/s/7AHS5AlY2OaiLO4ag6zVMQ)
- [知识总结：一举拿下网络IO模型](https://mp.weixin.qq.com/s/vwJ5T7x1Jv4wa_x7nwr54Q)
- [知识总结：用一个故事来感受一下什么叫NIO](https://mp.weixin.qq.com/s/iFIt8t-q6hU-Ghwlj0hohw0)
- [知识总结：多路复用、非阻塞、线程与协程](https://mp.weixin.qq.com/s/xIfxzxCfbvBvQ2c0aUuA0A)
- [知识总结：搞懂原码/反码/补码](https://mp.weixin.qq.com/s/laP7Vxl6o40mIYBS7Cuj7A)
- [知识总结：30张图解高并发服务模型哪家强](https://mp.weixin.qq.com/s/a2Q1DQqOHdhtGEjJ4QxPew)
- [原理探究：新建一个空文件到底会占用多少磁盘空间？](https://mp.weixin.qq.com/s/vMvAgo9IWaLtDwxW1627YA)
- [面试相关：2.5w字+36张图+1周时间：爆肝操作系统面试题！](https://mp.weixin.qq.com/s/lR_A3jbfRiRjchz_lyyKxw)
- [面试相关：1.3w字的操作系统高频面试题大分享](https://mp.weixin.qq.com/s/oTEMOQY1xcG8uVceW-kLDA)
- [面试相关：学完操作系统内存管理，能回答这8个问题吗](https://mp.weixin.qq.com/s/5f3ku77xSO8UlaOsTkOnHQ)

---

## 数据库/SQL

- [原理探究：详解一条SQL的执行过程](https://mp.weixin.qq.com/s/OnGaqyUpB58pC2rqqzIzgw)
- [原理探究：MySQL的数据存在磁盘上到底长什么样](https://mp.weixin.qq.com/s/36Jaj79Y8BxFoDB3Bwe7mg)
- [原理探究：数据库索引到底是什么](https://mp.weixin.qq.com/s/WMuxdG3ymNMWWDk1XhLwZQ)
- [原理探究：为什么MySQL索引要用B+ tree](https://mp.weixin.qq.com/s/d7Zfat2fP6IX5DMKKtEIjQ)
- [原理探究：为什么用了索引，SQL查询还是慢](https://mp.weixin.qq.com/s/-mOOchaXx_pKI6qWhohEnA)
- [原理探究：数据库索引的原理和使用准则](https://mp.weixin.qq.com/s/4K3borSZXt-yc5t5UJnJpQ)
- [原理探究：为什么你写的SQL那么慢](https://mp.weixin.qq.com/s/iBnav_WPrX5vjR-NbdNeQg)
- [原理探究：count(1)和count(*)到底哪个效率高](https://mp.weixin.qq.com/s/lyAFcnZIoyhACw78Nf9xQg)
- [原理探究：为什么阿里规定超过三张表禁止join](https://mp.weixin.qq.com/s/7vN9Nf20NGnvLKALHw_O1Q)
- [原理研究：什么是MySQL索引下推？](https://mp.weixin.qq.com/s/aPVs9Jrk07KsXcXdPOpx5A)
- [原理探究：为什么大公司后台数据库都要搞分库分表？](https://mp.weixin.qq.com/s/yflzIQFiNa3tDJm7U9P8ig)
- [原理探究：MySQL不会丢失数据的奥秘就藏在这里](https://mp.weixin.qq.com/s/QBeyJz2gVq1p7wBxcY1Gfw)
- [原理探究：MySQL主从复制那些事儿](https://mp.weixin.qq.com/s/CCLsmKSsodtkz4iX84Cdig)
- [实践应用：常见的SQL错误（不当）写法例析](https://mp.weixin.qq.com/s/caBYeVtZvNzbSs4q-6710Q)
- [实践应用：SQL优化的几个角度](https://mp.weixin.qq.com/s/hl11JYMwl30FsDVZ40CLVQ)
- [实践应用：数据库、数据表设计规范例析](https://mp.weixin.qq.com/s/hE2uKE2ffNCmeHLRn2KSTQ)
- [实践应用：梳理开发中常用的SQL优化途径](https://mp.weixin.qq.com/s/jl0j-T6XldN6Nq-jYoQ-gA)
- [实践应用：先更新数据库还是先更新缓存？](https://mp.weixin.qq.com/s/SPgtpfgv6bz2AfPa1CYYeQ)
- [实践应用：百亿级数据分表后如何分页查](https://mp.weixin.qq.com/s/EplL3kBx5vOXGDhDOP8NjQ)
- [实践应用：什么是SQL注入攻击](https://mp.weixin.qq.com/s/mnZT0Z5L6Hi6gRgEO1C9tg)
- [实践应用：用对这些场景下的数据库索引，领导说我有点东西](https://mp.weixin.qq.com/s/4K3borSZXt-yc5t5UJnJpQ)
- [实践应用：一个遗留项目的SQL优化实战录](https://mp.weixin.qq.com/s/MA7FVeJDMg8WDJABiBWpBA)
- [实践应用：误删数据库后该如何恢复](https://mp.weixin.qq.com/s/UYZZkrbAetgnPUjGa71fJA)
- [实践应用：如何科学根治慢SQL？](https://mp.weixin.qq.com/s/eQKphrkPeN_-EcWIxETz9Q)
- [面试相关：面试官最爱的数据库索引连环问](https://mp.weixin.qq.com/s/MLvJsJuFAHHcllqvk1nVRQ)
- [面试相关：30道保底的MySQL数据库面试题集合](https://mp.weixin.qq.com/s/aBboeqEphejICklAKLqS2Q)
- [面试相关：数据库自增ID用完了会怎样](https://mp.weixin.qq.com/s/WqM5mhnLOqZhcdzPLeWh5w)
- [面试相关：如何保证缓存和数据库的一致性问题？](https://mp.weixin.qq.com/s/RDOKLnG7P1j5Ehu3EyrsCQ)
- [面试相关：面试官最爱的MySQL连环问](https://mp.weixin.qq.com/s/8ddEzG-NzzFD35ehvbER7A)
- [面试相关：如何实现丝滑的数据库扩容](https://mp.weixin.qq.com/s/1VCC3i6ZCk7sb9kVRZ1czQ)

---

## 设计模式

  - [Golang实现各种设计模式代码](https://gitlab.com/893376179/golang_design_parttern)
  - [资源下载 → 23种设计模式学习笔记PDF文档](https://mp.weixin.qq.com/s/miV4SH1Eb_FMogQ2XYvPuA)
  - [单例模式详解](https://mp.weixin.qq.com/s/dXUgiMacKjwTBwX16cXNBA)
  - [工厂模式讲解, 引入Spring IOC](https://mp.weixin.qq.com/s/8TfJ1uhMiKHnTmlF9D0L2Q)
  - [什么是动态代理模式](https://mp.weixin.qq.com/s/GT1-yrxJ5KF0xeMydbJDCQ)
  - [经典面试题：Spring用到了哪些设计模式](https://mp.weixin.qq.com/s/ZcKqGT2Sn4wVScExSF9Alg)

---

## 架构及系统调优

- [基础设施及系统层网络调优思路](https://mp.weixin.qq.com/s?__biz=MzU3OTg2NDE0Mw==&mid=2247483776&idx=1&sn=7dcad5c9dda5f8d60e8d879fe91dd6bc&chksm=fd5ed2a0ca295bb68664fd025ce83ffa36f253b147935ad49bd9956604cbf5eb01a53bf84f62&scene=178&cur_album_id=1830133719764910081#rd)
- [TLS/SSL性能优化思路](https://mp.weixin.qq.com/s?__biz=MzU3OTg2NDE0Mw==&mid=2247484121&idx=1&sn=fc410f7257e24c4c85695f9daa346c8e&chksm=fd5ed1f9ca2958ef4ad41caecfb7b7c4f49881b019a069a5cfcb44d07cb6075adb7140159b7a&token=2137302191&lang=zh_CN#rd)
- [应用层编解码HTTP协议调优思路](https://mp.weixin.qq.com/s?__biz=MzU3OTg2NDE0Mw==&mid=2247484134&idx=1&sn=9c67b60b1d9f64292906f48db835f767&chksm=fd5ed1c6ca2958d0fa3da40e357cf2d8c115699eaeb0fa3ce3a7225aa4f52dffbee6d08792dc&token=2137302191&lang=zh_CN#rd)
- [最常用的架构模式](https://mp.weixin.qq.com/s/M0anfBkjTJT8ixlMNEPfxg)
- [go-zero是如何解决数据一致性问题的](https://mp.weixin.qq.com/s/DhIv9RACxa5igJTYg4N1mA)
- [gRPC 客户端长连接机制实现](https://mp.weixin.qq.com/s/dHqgPFrOXumKOoc84brTZg)
- [一个生产就绪的微服务需要考虑哪些问题](https://mp.weixin.qq.com/s/rwc_PPsyr0Mo0zQQ70xELw)
- [并发场景下的幂等问题——分布式锁详解](https://mp.weixin.qq.com/s/JwB23q4f7dYw9Ig_66JypA)
- [“12306” 的架构到底有多牛逼？](https://mp.weixin.qq.com/s/ZWFTQFYkEQhwDp9L8ZFF5A)

---

## 其他

- [每个开发人员都应该了解一点的UML规范](https://mp.weixin.qq.com/s/7-CTSWN-VLYgPH5H2nPU8w)
- [手工模拟实现 Docker 容器网络！](https://mp.weixin.qq.com/s/Arcz3RWe_o0Ijw6uPWKdVw)
- [计算机时间到底是怎么来的](https://mp.weixin.qq.com/s/VFUg1S0ApuFzlTNYSCLkMQ)
- [乱码问题科普：“锟斤拷”的前世今生~](https://mp.weixin.qq.com/s/kTADQtTeOWPuXsvR9HsUIg)
- [从一个面试题看程序员的几项基本功](https://mp.weixin.qq.com/s/dVgDv1bNH8ivO0Ft0FtmZg)
- [卡牌类游戏游戏大厅(上)](https://mp.weixin.qq.com/s?__biz=MzU3OTg2NDE0Mw==&mid=2247484596&idx=1&sn=c5071dec23d5894f8a1aadf755a25cad&chksm=fd5ed794ca295e82265ada63129e46716d578e4f0e2882f9e6ba69075ed141e7f9f087a0753c&token=2137302191&lang=zh_CN#rd)
- [卡牌类游戏游戏大厅(下)](https://mp.weixin.qq.com/s?__biz=MzU3OTg2NDE0Mw==&mid=2247484617&idx=1&sn=5098bc834ec18118bf1b410c728820be&chksm=fd5ed7e9ca295eff075dd63aba77226fbbdda5b28739470db45dade2ef4759e4bd2ea944e767&token=2137302191&lang=zh_CN#rd)

---
# **应用框架和工具**

---
> [!NOTE]
>  - 计算机基础聊完了，接下来就是应用和实践的环节了。这部分通常会涉及到一些**工具**、**编程环境**、以及**应用框架**。
>  
>  - 企业级开发不同于个人自学，出于**可复用性**、**稳定性**、**开发成本**、**开发效率**、**质量保证**等一系列因素的考量，不可能每一个功能、每一个组件都从0开始徒手造轮子，所以这时候各种应用框架和工具的出现就非常有帮助了。
>  
>  - 其实框架讲白了就是别人经过多年迭代写好的一套**工具**、**代码库**、**逻辑**、亦或是**流程**，把原本都需要从0开始手写的基本功能和组件都给封装进去，用户只要符合框架约定的规则进行编码，或者调用框架提供的方法或者工具，就能快速拉起业务功能，创造实际经济价值。
>  
>  - 所以框架的出现从不同的角度看也是有多面性的：对于企业级开发效率提升而言肯定是有帮助的，但是对于程序员个人思考编码能力提升却未必是好事，所以多注重基础、深挖原理，提升自身竞争力还是非常有必要的。



## 前后端分离

- [概念科普：到底什么是RESTful ？](https://mp.weixin.qq.com/s/um5kDYBscf5sy7FUhmP7ww)
- [概念科普：RESTful风格的前世今生](https://mp.weixin.qq.com/s/lrFQUEITm_3zASIBR1mcQw)
- [概念科普：为什么需要前后端分离？](https://mp.weixin.qq.com/s/Y0z-3r_Mdm-etCwa0GmQmQ)
- [实践应用：前后端分离的接口规范，我们是如何做的？](https://mp.weixin.qq.com/s/zAOYAcR-6DEJU_s0qXe91g)
- [实践应用：前后端分离式开发：高效协作10条准则](https://mp.weixin.qq.com/s/niYHlrCDIkA1NqPVI_VJ8w)
- [实践应用：前后端分离后，接口联调总是甩锅怎么办？](https://mp.weixin.qq.com/s/TqSQ21nNvV5WwWDY_WItQA)
- [实践应用：前后端分离项目接口优雅数据交互例析](https://mp.weixin.qq.com/s/B-mJjBeJy5W7s2O79Iitqg)
- [实践应用：前后端分离实践中常遇到的跨域问题](https://mp.weixin.qq.com/s/Sjrgf3Tp3vR5zsNIiYzdpA)
- [工具推荐：前后端都分离了，该搞个好用的API管理系统了](https://mp.weixin.qq.com/s/Ahs6fnIfFVVPOn3NZpIsNA)

<!-- div:right-panel -->

## 微服务框架

- [科普篇：企业里常用的软件架构剖析](https://mp.weixin.qq.com/s/NJFJ5UrzGk0cMGL1oZe0Bg)
- [科普篇：单体→分布式→微服务，这些年的软件架构是如何发育的](https://mp.weixin.qq.com/s/kfhCEDSUGwnNUbtGDL_rvQ)
- [科普篇：微服务架构的全局图景分析](https://mp.weixin.qq.com/s/Iy1dguNFkU73r1FxYIvlIQ)
- [科普篇：分布式架构负载均衡的几种方式](https://mp.weixin.qq.com/s/8CvoGCtGDvMHR91Lx89pxQ)
- [科普篇：RPC 技术(框架) 大科普](https://mp.weixin.qq.com/s/iTnKumgre3SzrBuWf8KB6g)
- [科普篇：从一次RPC调用流程看各场景下gRPC框架的解决方案](https://mp.weixin.qq.com/s?__biz=MzU3OTg2NDE0Mw==&mid=2247484165&idx=1&sn=2a26423fce145e5d2f21aefc3ded5ec0&chksm=fd5ed025ca2959339b2c3b4a88b5ce1e89ddb3ab968ac51e97373285b8cae5cb5c780c74b81e&token=2137302191&lang=zh_CN#rd)
- [原理探究：什么是分布式事务](https://mp.weixin.qq.com/s/_56jq_p_nDUiBwaI2MTlmA)
- [原理探究：Redis分布式锁保姆级无死角分析](https://mp.weixin.qq.com/s/RViDM1WHE61SDLNKzUmTAg)
- [原理探究：如何设计一个高并发的秒杀架构？](https://mp.weixin.qq.com/s/fLLd-ml-gdo3-700iJso_A)
- [原理探究：亿级流量网关设计思路](https://mp.weixin.qq.com/s/J0aSVry1-Ss1OTA-jQAX3w)
- [原理探究：分布式架构的几个常见的坑](https://mp.weixin.qq.com/s/pg6lyAkgAaqSAZjiPgQTWA)
- [实践应用：RPC框架实践之：Apache Thrift](https://mp.weixin.qq.com/s/iCw-CHa5ITz6zoQI78Wb3w)
- [实践应用：RPC框架实践之：Google gRPC](https://mp.weixin.qq.com/s/LdzkBUzvyjMVW7pgzkA23w)
- [实践应用：Spring Cloud Feign的两种使用姿势](https://mp.weixin.qq.com/s/U4eustBBu9N-6B1SRsYc7w)
- [实践应用：Spring Cloud Eureka Server高可用之：在线扩容](https://mp.weixin.qq.com/s/oMsZquaphqEJqoGt1dt9WA)
- [实践应用：Eureka Server 开启Spring Security Basic认证](https://mp.weixin.qq.com/s/tRRwG1jnWi5r1XlbeBlv8g)
- [实践应用：Eureka Server启用 https服务指北](https://mp.weixin.qq.com/s/EAi7F3lBQ7ZrjSjNnFkj4g)
- [实践应用：微服务调用链追踪中心搭建](https://mp.weixin.qq.com/s/c55ejzWEU6SQBdcmTTRC1g)
- [实践应用：利用Zipkin追踪Mysql数据库调用链](https://mp.weixin.qq.com/s/wQRBxBBXCnP0YKPvtIPRcA)
- [实践应用：一个小团队的微服务架构改造之路](https://mp.weixin.qq.com/s/VjBiUmQNQPpSHeSVjK1C2A)
- [官方文档：go-zero：一个集成了各种工程实践的 web 和 rpc 框架](https://go-zero.dev/cn/docs/quick-start/monolithic-service)
- 内容持续更新中...

<!-- div:left-panel -->

## 容器/集群

- [知识总结：常用虚拟化技术（VMware/KVM/Docker）对比](https://mp.weixin.qq.com/s/hVNMfh9nWdilXHSqvyS9ng)
- [知识总结：Docker从入门到干活，看这一篇入门](https://mp.weixin.qq.com/s/YlcvlUQ-xkz25PuYkeEQqw)
- [知识总结：Docker Swarm的前世今生](https://mp.weixin.qq.com/s/jf4qWaEYfn0CH5A6fG6MKw)
- [知识总结：编写高效Dockerfile的几条准则](https://mp.weixin.qq.com/s/MDGvWKkk4npWtqo2k4tsfg)
- [知识总结：Kubernetes微服务常见概念及应用](https://mp.weixin.qq.com/s?__biz=MzU3OTg2NDE0Mw==&mid=2247484794&idx=1&sn=fc6f725c8d9f744d9a379211c10757b8&chksm=fd5ed65aca295f4cd3137c127275da0bcff6aa0abb398bd1a0409cf4fd0d09f1bee0dc4fae01&token=2137302191&lang=zh_CN#rd)
- [实践应用：Docker Swarm集群初探](https://my.oschina.net/hansonwang99/blog/1603378)
- [实践应用：利用ELK搭建Docker容器化应用日志中心](https://mp.weixin.qq.com/s/sO9_TixS0XrKoCKIS9oETw)
- [实践应用：Docker容器可视化监控中心搭建](https://mp.weixin.qq.com/s?__biz=MzU4ODI1MjA3NQ==&mid=2247483763&idx=1&sn=6ceb9e73540b5016dadfb212636b3855&scene=21#wechat_redirect)
- [实践应用：Docker容器跨主机通信之直接路由方式](https://mp.weixin.qq.com/s/evxiE6nhK6_y6zLMFWHTKQ)
- [实践应用：利用TICK搭建Docker容器可视化监控中心](https://mp.weixin.qq.com/s/-54IZbsX2nQIHaMZ1y6fTQ)
- [实践应用：利用Kubeadm部署Kubernetes 1.13.1集群实践录](https://mp.weixin.qq.com/s/wx68Chw1XaKQ1cb57jTiOw)
- [实践应用：利用K8S技术栈打造个人私有云（连载之：初章）](http://mp.weixin.qq.com/s?__biz=MzU4ODI1MjA3NQ==&mid=2247483699&idx=1&sn=57b84f4ec72c8a578934cdb4225e6fe7&chksm=fdded7f7caa95ee198652c295b48b74565fd244afc4dccc0551b036c8216caab0397a1342d99&scene=21#wechat_redirect)
- [实践应用：利用K8S技术栈打造个人私有云（连载之：K8S集群搭建）](http://mp.weixin.qq.com/s?__biz=MzU4ODI1MjA3NQ==&mid=2247483755&idx=1&sn=92a547c579aeacf1db9a8f0e56601b52&chksm=fdded7afcaa95eb9d6aa6cf323bff1df46692fc6a6672e0a483105f091ddf9670fb06d034e2a&scene=21#wechat_redirect)
- [实践应用：利用K8S技术栈打造个人私有云（连载之：K8S环境理解和练手）](http://mp.weixin.qq.com/s?__biz=MzU4ODI1MjA3NQ==&mid=2247483758&idx=1&sn=629b6219a06374b2050703b9549037fa&chksm=fdded7aacaa95ebc9a3e88e839515b13109752c26c586032ae60219d2c3b24e13385e1bcd37c&scene=21#wechat_redirect)
- [实践应用：利用K8S技术栈打造个人私有云（连载之：基础镜像制作与实验）](http://mp.weixin.qq.com/s?__biz=MzU4ODI1MjA3NQ==&mid=2247483759&idx=1&sn=8403d42fe1769a252f9ee2997fc2f561&chksm=fdded7abcaa95ebd8cdae86f4d692f1f6e92daab74dbbd8c56c6db8fe6aa2a3616ccce1eebb1&scene=21#wechat_redirect)
- [实践应用：利用K8S技术栈打造个人私有云（连载之：K8S资源控制）](http://mp.weixin.qq.com/s?__biz=MzU4ODI1MjA3NQ==&mid=2247483760&idx=1&sn=d44f2c744ccdc965fb6c7e4d9efe9f92&chksm=fdded7b4caa95ea278d7363b4b4788d84914b915422a9cc98e7936c3bce0c7ce1197abfac13f&scene=21#wechat_redirect)
- 内容持续更新中...

<!-- div:right-panel -->

## 缓存/MQ/中间件/服务器

- [缓存：Redis字符串类型内部编码剖析](https://mp.weixin.qq.com/s/tL2HVoeEH9hnalb9-tuoXg)
- [缓存：Redis哈希结构内存模型剖析](https://mp.weixin.qq.com/s/TdPcIMnv4iKIx3dRjei95A)
- [缓存：15张图解：为什么Redis这么快](https://mp.weixin.qq.com/s/0R0Evh1QX5BPOQt9233vpQ)
- [缓存：Redis面试全攻略、面试题大集合](https://mp.weixin.qq.com/s/6NobACeeKCcUy98Ikanryg)
- [缓存：把Redis当作队列来用，真的合适吗？](https://mp.weixin.qq.com/s/aNdWL3xKtjOd_briXL6Bjg)
- [缓存：缓存和数据库一致性问题，看这个就够了](https://mp.weixin.qq.com/s/RDOKLnG7P1j5Ehu3EyrsCQ)
- [缓存：面试官爱问的Redis高频面试题大集合](https://mp.weixin.qq.com/s/lxMP4-Z3DzQg5fRqLs9XNA)
- [缓存：1分钟科普：什么是缓存穿透、缓存雪崩、缓存击穿？](https://mp.weixin.qq.com/s/m9trzqE3Zd0KHC1cA3PlZQ)
- [缓存：如何构建一个稳定的高性能Redis集群？](https://mp.weixin.qq.com/s/x1AobPWpMufNWqrBnc5sZg)
- [缓存：轻量级memcached缓存代理twemproxy初探实践](https://mp.weixin.qq.com/s/Fgg2bNhdOTRfHIzGX86X3Q)
- [缓存：先更新数据库还是先更新缓存](https://mp.weixin.qq.com/s/SPgtpfgv6bz2AfPa1CYYeQ)
- [MQ:消息丢失、重复消费、消费顺序、堆积、事务、高可用...](https://mp.weixin.qq.com/s/yiXBFgIxJqQnT7A-Oeit7g)
- [MQ：消息队列RabbitMQ快速入门上手](https://mp.weixin.qq.com/s/WMriUm27CZkiSWEt1f4lig)
- [MQ：Kafka快速入门上手](https://mp.weixin.qq.com/s/bUNAD2fkGTD73jVLw8rDHg)
- [MQ：从面试角度一文学完Kafka](https://mp.weixin.qq.com/s/o-rqnOH4FHeHaz0VqoHnFg)
- [MQ：为什么Kafka用得这么多？](https://mp.weixin.qq.com/s/vC0YbMJxHqs0JfcdAxjhHg)
- [MQ：刨根问底之Kafka到底会不会丢消息](https://mp.weixin.qq.com/s/PDvG5vSuA_NCkJ3mMUua5w)
- [MQ：大厂面试必备之消息队列连环问](https://mp.weixin.qq.com/s/u6_WH-r1bRc4m7CUm21Tew)
- [MQ：后端岗面试必备之Dubbo九连问](https://mp.weixin.qq.com/s/wM0Vj_YMh7881NwK-AwKSw)
- [服务器：Nginx服务器开箱体验](https://mp.weixin.qq.com/s/mif0NmY1Ij6lapHppb6LFg)
- [服务器：从一份配置清单详解Nginx服务器配置](https://mp.weixin.qq.com/s/099PJZE89JVNnaF0kwePDQ)
- [服务器：为什么Nginx能轻松撑起几万并发](https://mp.weixin.qq.com/s/VR1jEhJVXCfja3Pmo_kKkw)
- [搜索引擎：CentOS7上搭建多节点Elasticsearch集群](https://mp.weixin.qq.com/s/pdQNDMQQnXpoVKIA8JpyQQ)
- [搜索引擎：一文上手Elasticsearch常用可视化管理工具](https://mp.weixin.qq.com/s/BE8LrpviJNXGV41bhzGFTw)
- [搜索引擎：从一份定义文件详解ELK中Logstash插件结构](https://mp.weixin.qq.com/s/d2maG61HB2_rU56DYD2A9w)
- [搜索引擎：Elasticsearch索引的映射配置详解](https://mp.weixin.qq.com/s/C8JTxdjeSCR22xeAiLHHGg)
- [其他中间件：ZooKeeper面试常见十二问](https://mp.weixin.qq.com/s/ir0uurwo95hB3g__vTceJQ)
- [其他中间件：基于代理的数据库分库分表框架Mycat实践](https://mp.weixin.qq.com/s/3L5PcA0dGlrqJ2FjM_C77Q)
- 内容持续更新中...

<!-- div:left-panel -->


<!-- panels:end -->




---
# **项目经验获得**

## 自学党如何获得项目经验？

<p align="center">

<iframe src="//player.bilibili.com/player.html?aid=541205956&bvid=BV1Ai4y1V7PX&cid=208099724&page=1&as_wide=1&high_quality=1&danmaku=1" allowfullscreen="true" width="100%" height="450" scrolling="no" frameborder="0" sandbox="allow-top-navigation allow-same-origin allow-forms allow-scripts"></iframe>

</p>

<!-- panels:start -->

<!-- div:left-panel -->

## 开源项目

- [开源协议：开源软件的许可协议，先了解一下](https://mp.weixin.qq.com/s/ojGRSWSaJutAsBC0i5_6jA)
- [Golang开源项目：有哪些不错的golang开源项目](https://www.zhihu.com/question/48821269)
- [Java开源项目：看完这篇，别人的Java开源项目结构应该能看懂](https://mp.weixin.qq.com/s/5Ar5B9Ah2BdO8i9YjMQ7Qg)
- [Java开源项目：Java领域学习和练手的开源项目分享：第一弹](https://mp.weixin.qq.com/s/PmxVlkI9LUmnqqSk0Frqeg)
- [Java开源项目：Java领域学习和练手的开源项目分享：第二弹](https://mp.weixin.qq.com/s/y59hmDbQj1QglVj90viZqw)
- [Java开源项目：Java领域学习和练手的开源项目分享：第三弹](https://mp.weixin.qq.com/s/jjugnXIr3G0u7i9krbmmqg)
- [C/C++开源项目：几款适合初学者学习和练手的C/C++开源项目](https://mp.weixin.qq.com/s/bRNiqhZZXaoRwPEt8GIpLg)
- [C/C++开源项目：几款适合进阶学习和练手的C/C++开源项目](https://mp.weixin.qq.com/s/fNgOqKlITKa4yTbULO2Atw)
- [人工智能开源项目：AI领域几款标杆开源项目大巡礼](https://mp.weixin.qq.com/s/I9lr_SEg70OO_v-GOIUgjQ)
- [Python开源项目：几个炫酷的Python开源项目](https://mp.weixin.qq.com/s/Fu4OMojTJK0MrZohsLOx4w)
- [读源码的7大心得总结](https://mp.weixin.qq.com/s/eWN7I8eHGE2exrdF3xziIA)

<!-- div:right-panel -->

## 实际项目需求和问题例析

- [实践案例：实际项目业务接口的幂等性问题](https://mp.weixin.qq.com/s/gqvgysyGcP3yr0of_GwaTw)
- [实践案例：URL短链接服务如何设计](https://mp.weixin.qq.com/s/KPUuCU-q1SqnHzeyQeQzhw)
- [实践案例：亿行数据超大文件如何高效导入生产数据库](https://mp.weixin.qq.com/s/A-ZwcxlkB7Groe4k4Vq1Ew)
- [实践案例：订单系统一般怎么设计](https://mp.weixin.qq.com/s/-3g1ljoIrYWhhTHTF2RmLw)
- [实践案例：百亿级数据分表后如何分页查](https://mp.weixin.qq.com/s/EplL3kBx5vOXGDhDOP8NjQ)
- [实践案例：线上服务的YGC问题排查例析](https://mp.weixin.qq.com/s/-8xYoAkBUoavcSl69I0XJw)
- [实践案例：线上服务的FGC问题排查例析](https://mp.weixin.qq.com/s/Sjh9qFPKF250vLQGZig6nw)
- [实践案例：一个小团队的微服务架构改造之路](https://mp.weixin.qq.com/s/VjBiUmQNQPpSHeSVjK1C2A)
- [实践案例：一个遗留项目的SQL优化实战录](https://mp.weixin.qq.com/s/MA7FVeJDMg8WDJABiBWpBA)
- [实践案例：分布式锁使用不当导致的业务损失例析](https://mp.weixin.qq.com/s/OhkdQJ10biLOIezXc_vhkA)
- [实践案例：定时任务的几种简单方案](https://mp.weixin.qq.com/s/0p44f213fYdtr5LPJlrfLQ)
- [实践案例：项目常用的6种URL去重方案汇总](https://mp.weixin.qq.com/s/3zXdH2MALmWj57MWt0dImw)
- [实践案例：分布式系统架构的几个常见的坑](https://mp.weixin.qq.com/s/pg6lyAkgAaqSAZjiPgQTWA)
- [项目架构科普：企业里常用的软件架构剖析](https://mp.weixin.qq.com/s/NJFJ5UrzGk0cMGL1oZe0Bg)
- [项目架构科普：单体→分布式→微服务，这些年的软件架构是如何发育的](https://mp.weixin.qq.com/s/kfhCEDSUGwnNUbtGDL_rvQ)
- [项目架构科普：微服务架构的全局图景分析](https://mp.weixin.qq.com/s/Iy1dguNFkU73r1FxYIvlIQ)
- [原理解析：如何设计一个高并发的秒杀架构？](https://mp.weixin.qq.com/s/fLLd-ml-gdo3-700iJso_A)
- [原理解析：高并发系统设计的常见问题小结](https://mp.weixin.qq.com/s/ZzxmDbs08waUCtO7PeUhqw)
- [原理解析：亿级流量网关设计思路](https://mp.weixin.qq.com/s/J0aSVry1-Ss1OTA-jQAX3w)
- [原理解析：实际网站用户密码是如何存储的](https://mp.weixin.qq.com/s/Fd3cUDrqr0CsCl7FvZgd7Q)
- [原理解析：死磕搜索引擎背后的故事](https://mp.weixin.qq.com/s/Q1rZYvgklC_mBqfmIEC0Dw)
- [原理解析：用微信扫码登录的背后发生了什么](https://mp.weixin.qq.com/s/1lG-aAyVycO4zTbOqdy6BA)
- [原理解析：扫码付款背后的原理解析](https://mp.weixin.qq.com/s/RjVLsAhiMIYzsMhXJiReNg)
- [原理解析：收款码背后的原理剖析](https://mp.weixin.qq.com/s/1b9jBfwM9_RLkMPquqAuMw)
- [原理解析：支付掉单背后的几个问题](https://mp.weixin.qq.com/s/Kske9ahRcIUM5XEs2ZIN3A)
- [原理解析：手机没网却能支付成功的背后原理](https://mp.weixin.qq.com/s/VROp_0M3mz5Zhhnz0ZSmKQ)
- [原理解析：高并发场景下，先更新缓存还是先更新数据库？](https://mp.weixin.qq.com/s/1FDb63R_OMbSJo9VOWSr3Q)
- [原理解析：如何保证缓存和数据库的一致性问题？](https://mp.weixin.qq.com/s/RDOKLnG7P1j5Ehu3EyrsCQ)
- [原理解析：什么是分布式事务？](https://mp.weixin.qq.com/s/_56jq_p_nDUiBwaI2MTlmA)
- [原理解析：Redis分布式锁实践分析](https://mp.weixin.qq.com/s/RViDM1WHE61SDLNKzUmTAg)
- [原理解析：高并发下如何保证接口的幂等性？](https://mp.weixin.qq.com/s/QgliAGeUAISQVR4VNWqdgA)
- [原理解析：如何实现丝滑的数据库扩容](https://mp.weixin.qq.com/s/1VCC3i6ZCk7sb9kVRZ1czQ)
- [原理解析：如何构建一个稳定的高性能Redis集群？](https://mp.weixin.qq.com/s/x1AobPWpMufNWqrBnc5sZg)
- 内容持续更新中...


<!-- panels:end -->

---
# **面试准备和求职**

---
> [!NOTE]
>  该板块会持续更新各技术方向的求职面试题集合、面经、以及一些小伙伴的求职心得与经历感悟。

## 简历相关

- [资源下载 → 简历模板下载（word版）](https://mp.weixin.qq.com/s/-qlU2-a-vvXWOHXzKHRm6A)
- [程序员写简历时必须注意的技术词汇拼写](https://mp.weixin.qq.com/s/q5gPSIiJqVvbI6Xa4dMXwA)
- [国内程序员最容易发音错误的单词集合](https://mp.weixin.qq.com/s/VJsp5SzGuOCGl-mWdbtCxQ)

## 面试题集合

<!-- panels:start -->

<!-- div:left-panel -->
- [Gopher反向面试，这份宝典不得不阅](https://mp.weixin.qq.com/s/MwEULNRu8pDckQx96QLLEQ)
- [计网：面试最爱问的三次握手和四次挥手问题](https://mp.weixin.qq.com/s/lFnyBaaP3f0eNcKGW5RtCg)
- [计网：计算机网络高频12问](https://mp.weixin.qq.com/s/PIGfhMaAx9R5C4x5eJQnIw)
- [计网：GET和POST到底有什么区别](https://mp.weixin.qq.com/s/H4gbg7bfnw61jZQcapTnMw)
- [计网：TCP/UDP 部分高频面试题大集合](https://mp.weixin.qq.com/s/doxVJZ1G6187B4AOXb0JlA)
- [计网：TCP经典15连问！](https://mp.weixin.qq.com/s/uMVTMkXVxOczjdXZR_7JbQ)
- [计网：HTTP常见面试题集合](https://mp.weixin.qq.com/s/FJGKObVnU61ve_ioejLrtw)
- [计网：浏览器输入网址并回车，后面到底发生了什么](https://mp.weixin.qq.com/s/t2Csrl7idUXISKW54cpV1g)
- [计网：都说HTTP协议是无状态的，这里的「状态」到底指什么？](https://mp.weixin.qq.com/s/EZwOUGMrGKEF_POisJKmuw)
- [操作系统：操作系统面试题集合](https://mp.weixin.qq.com/s/lR_A3jbfRiRjchz_lyyKxw)
- [操作系统：内存管理部分8个常见面试题汇总](https://mp.weixin.qq.com/s/5f3ku77xSO8UlaOsTkOnHQ)
- [操作系统：操作系统面试题大分享](https://mp.weixin.qq.com/s/oTEMOQY1xcG8uVceW-kLDA)
- [操作系统：面试常问的20个Linux命令](https://mp.weixin.qq.com/s/3NI4FWuOfYMJBiKqqnpqMA)

<!-- div:right-panel -->

- [数据库：MySQL面试硬核25问（附答案）](https://mp.weixin.qq.com/s/vdOOVQtZhrJXsvRUjq0HqQ)
- [数据库：先更新数据库还是先更新缓存？](https://mp.weixin.qq.com/s/SPgtpfgv6bz2AfPa1CYYeQ)
- [数据库：如何保证缓存和数据库的一致性问题？](https://mp.weixin.qq.com/s/RDOKLnG7P1j5Ehu3EyrsCQ)
- [数据库：面试官最爱的MySQL连环问](https://mp.weixin.qq.com/s/8ddEzG-NzzFD35ehvbER7A)
- [数据库：如何实现丝滑的数据库扩容](https://mp.weixin.qq.com/s/1VCC3i6ZCk7sb9kVRZ1czQ)
- [数据库：数据库索引，小白连环16问](https://mp.weixin.qq.com/s/sXMYVK3hKQyi8-4ip44Zmw)
- [数据库：数据库索引到底是什么](https://mp.weixin.qq.com/s/WMuxdG3ymNMWWDk1XhLwZQ)
- [数据库：为什么MySQL索引要用B+ tree](https://mp.weixin.qq.com/s/d7Zfat2fP6IX5DMKKtEIjQ)
- [数据库：为什么用了索引，SQL查询还是慢](https://mp.weixin.qq.com/s/-mOOchaXx_pKI6qWhohEnA)
- [数据库：慢SQL背后的原理是什么](https://mp.weixin.qq.com/s/iBnav_WPrX5vjR-NbdNeQg)
- [数据库：30道保底的MySQL数据库面试题汇总](https://mp.weixin.qq.com/s/aBboeqEphejICklAKLqS2Q)
- [数据库：用心整理的9道MySQL面试题](https://mp.weixin.qq.com/s/JQCtqM6aep3jtgiRL_9J5g)
- [数据库：聊聊sql优化的15个小技巧](https://mp.weixin.qq.com/s/htpUdtrIkGnR6Kx0ffWSQQ)
- [中间件：面试官爱问的Redis高频面试题集合](https://mp.weixin.qq.com/s/lxMP4-Z3DzQg5fRqLs9XNA)
- [中间件：Redis面试全攻略、面试题大集合](https://mp.weixin.qq.com/s/6NobACeeKCcUy98Ikanryg)
- [中间件：什么是缓存穿透、缓存雪崩、缓存击穿？](https://mp.weixin.qq.com/s/m9trzqE3Zd0KHC1cA3PlZQ)
- [中间件：说说为什么Redis能这么快？](https://mp.weixin.qq.com/s/0R0Evh1QX5BPOQt9233vpQ)
- [中间件：如何构建一个稳定的高性能Redis集群？](https://mp.weixin.qq.com/s/x1AobPWpMufNWqrBnc5sZg)
- [中间件：把Redis当作队列来用，真的合适吗？](https://mp.weixin.qq.com/s/aNdWL3xKtjOd_briXL6Bjg)
- [中间件：Redis分布式锁的详细分析](https://mp.weixin.qq.com/s/RViDM1WHE61SDLNKzUmTAg)
- [中间件：ZooKeeper面试十二连问](https://mp.weixin.qq.com/s/ir0uurwo95hB3g__vTceJQ)
- [中间件：消息队列面试连环问总结](https://mp.weixin.qq.com/s/xpSel2nsLkf3yck5edNMJQ)
- [中间件：Kafka面试要点梳理](https://mp.weixin.qq.com/s/o-rqnOH4FHeHaz0VqoHnFg)
- [中间件：说说你对RPC技术是怎么理解的？](https://mp.weixin.qq.com/s/iTnKumgre3SzrBuWf8KB6g)
- [中间件：介绍下你如何理解Etcd](https://mp.weixin.qq.com/s?__biz=MzU3OTg2NDE0Mw==&mid=2247484736&idx=1&sn=7cff67234e025fc0b4c5eba94ad12b4e&chksm=fd5ed660ca295f76cbb96f71b9c5c88287edc20ec32b3e5417c5cc5b2b58f0a2d2175b9b2af3&token=2137302191&lang=zh_CN#rd)
- [中间件：为什么Nginx能轻松撑起几万并发？](https://mp.weixin.qq.com/s/VR1jEhJVXCfja3Pmo_kKkw)
- [中间件：用于日志收集的filebeat和logstash有了解过吗？(filebeat篇)](https://mp.weixin.qq.com/s?__biz=MzU3OTg2NDE0Mw==&mid=2247484753&idx=1&sn=859dfbd3cd7eba558b5c83cc33ad3d3b&chksm=fd5ed671ca295f674c1102a076b653407ac971e530e3f806da0ec49c3dc8f9beaeb9b81e4790&token=2137302191&lang=zh_CN#rd)
- [中间件：用于日志收集的filebeat和logstash有了解过吗？(go-stash篇)](https://mp.weixin.qq.com/s?__biz=MzU3OTg2NDE0Mw==&mid=2247484765&idx=1&sn=19b378123f8a1081a059dfa54997c8a5&chksm=fd5ed67dca295f6bb9ec6b807bcb912e2cb54e793bf0e3d32c698050944d36d5d6e53f102c11&token=2137302191&lang=zh_CN#rd)
- [系统架构：企业里常用的软件架构科普](https://mp.weixin.qq.com/s/NJFJ5UrzGk0cMGL1oZe0Bg)
- [系统架构：单体→分布式→微服务，这些年的软件架构是怎么发展的](https://mp.weixin.qq.com/s/kfhCEDSUGwnNUbtGDL_rvQ)
- [系统架构：如何设计一个高并发的秒杀架构？](https://mp.weixin.qq.com/s/fLLd-ml-gdo3-700iJso_A)
- [系统架构：分布式架构的几个常见的坑](https://mp.weixin.qq.com/s/pg6lyAkgAaqSAZjiPgQTWA)
- [系统架构：说说分布式架构负载均衡的几种方式](https://mp.weixin.qq.com/s/8CvoGCtGDvMHR91Lx89pxQ)
- [系统架构：说说什么是分布式事务？](https://mp.weixin.qq.com/s/_56jq_p_nDUiBwaI2MTlmA)
- [系统架构：亿级流量网关的设计思路](https://mp.weixin.qq.com/s/J0aSVry1-Ss1OTA-jQAX3w)

<!-- panels:end -->

## 求职经历分享

- [小伙伴经历分享：Linux后台开发该如何准备](https://mp.weixin.qq.com/s/joFBDntI9PmyVRiKPHwpRg)
- [二本无实习上岸滴滴京东58复盘(一)](https://mp.weixin.qq.com/s/V0nRwXQlkNBnBneERZQURQ)
- [二本无实习上岸滴滴京东58复盘(二)](https://mp.weixin.qq.com/s/BnbBQGL-S2sGdRtNh7DN1A)
- [小伙伴面经分享(一)：深信服(含答案)](https://mp.weixin.qq.com/s/3YfKHo2lW-NC1tEA_u-mlQ)
- [小伙伴面经分享(二)：京东(含答案)](https://mp.weixin.qq.com/s/sc-7rc6obYZNFcc1mjNPAg)
- [一位程序媛的秋招软件开发岗面试经历](https://mp.weixin.qq.com/s/jAPYcMULqQl6AE_Vv7BZhQ)
- [写简历→面试→谈薪：一绿向北](https://mp.weixin.qq.com/s/0RoFVAezxW8cl3bjlAHbQg)
- [小红书后台开发岗面试经历复盘~](https://mp.weixin.qq.com/s/4L3J45Wj81yjoZIQ3ZVSog)
- [国企和银行：面试都问些啥？如何准备？](https://mp.weixin.qq.com/s/34SdkDyMvpVmoCFEB4UF-g)
- [招银网络（银行科技岗）面试都问了些啥？](https://mp.weixin.qq.com/s/4hP8s7TzsOv_tdQtOdCdig)
- [准备提前批？还是去实习？还是准备秋招？](https://mp.weixin.qq.com/s/jy0g3ohLzbtD-6JeCeMVvw)
- [7面Google，面经分享](https://mp.weixin.qq.com/s/9Fb8w2XwkXQpj8agBtdmFg)
- [化学专业大二转码的抉择之路](https://mp.weixin.qq.com/s/j6poLkiomb80jTE7EaFVJA)
- [拿到腾讯实习Offer的经历分享](https://mp.weixin.qq.com/s/-27fvvzBpmwwuAFANRoeIA)
- [怎么准备国企和银行？](https://mp.weixin.qq.com/s/YP4qafQ8gPbB7W0CkfACug)
- [去银行当程序员是一种什么体验](https://mp.weixin.qq.com/s/u_jIpGfmH4BHYS7hIlQk9w)


## 城市/公司选择

- [一二线城市知名IT互联网公司总名单](https://mp.weixin.qq.com/s/VFocc914mGTrDzodI-MF1A)
- [常见互联网公司职级和薪资一览](https://mp.weixin.qq.com/s/5gtu7B05EKbPDXvhha5uNQ)
- [常见互联网公司时薪排行榜](https://mp.weixin.qq.com/s/wvdzBTcLLaeEtCqZI6T_Qg)
---
- [成都有哪些知名的IT/互联网公司？](https://mp.weixin.qq.com/s/UDWHt3QwsZBfZ25-pddwhQ)
- [武汉有哪些知名的IT/互联网公司？](https://mp.weixin.qq.com/s/bjHmAtPrSKQCDjfr9Mu0_Q)
- [南京有哪些知名的IT/互联网公司？](https://mp.weixin.qq.com/s/kskE2wJx_QtT6vCgsrUgIA)
- [合肥有哪些知名的IT/互联网公司？](https://mp.weixin.qq.com/s/tBNWFJbkjkV0bEPh4t9VQg)
- [深圳有哪些知名的IT/互联网公司？](https://mp.weixin.qq.com/s/dtJBi4QXXTrGkKTPM0y7VA)
- [成都有哪些知名的IT/互联网公司？](https://mp.weixin.qq.com/s/t4L45znsZRmWQ7MIQQtvhg)
- [西安有哪些知名的IT/互联网公司？](https://mp.weixin.qq.com/s/eD3tjUC4qASHH0WSWhz57Q)
- [长沙有哪些知名的IT/互联网公司？](https://mp.weixin.qq.com/s/UQHJ-iZDvQb3RLwQsELbUQ)
- [武汉有哪些知名的IT/互联网公司？](https://mp.weixin.qq.com/s/6dh1jbmuDqnHBoM7beTQbg)
- [天津有哪些知名的IT/互联网公司？](https://mp.weixin.qq.com/s/Ks9XLLUh099wSHWG0OBf1g)
- [郑州有哪些知名的IT/互联网公司？](https://mp.weixin.qq.com/s/8-uZUrG8_ziR5tKl1K8YNw)
- [杭州有哪些知名的IT/互联网公司？](https://mp.weixin.qq.com/s/Bwu0MDQhZiaY0vj3HsEsLg)
- [广州有哪些知名的IT/互联网公司？](https://mp.weixin.qq.com/s/s5UhdFyj9-8bqwQyZUMvVA)
- [青岛有哪些知名的IT/互联网公司？](https://mp.weixin.qq.com/s/Miu3eC6hqJlK8fi8Qr-DtQ)
- [上海有哪些知名的IT/互联网公司？](https://mp.weixin.qq.com/s/zYI8qW3Zfu2ySeO-PDqwZA)
- [大连有哪些知名的IT/互联网公司？](https://mp.weixin.qq.com/s/12inr2oRTULf7NhYOBGMYw)
- [苏州有哪些知名的IT/互联网公司？](https://mp.weixin.qq.com/s/529Zv24uToDW4xxEOFdiqw)

## 其他求职相关问题

- [为什么有些大公司技术却弱爆了？](https://mp.weixin.qq.com/s/QUJzl1ZG77DDnQNK0OAsHg)
- [嵌入式行业到底有没有搞头？](https://mp.weixin.qq.com/s/iV7DMApxhEw5YL-upIowVA)
- [找工作时，公司所说的「给期权」到底是什么东西？](https://mp.weixin.qq.com/s/9XMAvP_t0B1Ev57-ZC42yw)
- [入职一家公司，应该选择新业务还是老业务？](https://mp.weixin.qq.com/s/cu7mDeU2q2uEG8Q0CPzDEA)
- [采访了一位技术总监：聊了聊跳槽的心态问题](https://mp.weixin.qq.com/s/NGJc71cW2TKEONe6jhIUSg)
- [程序员从技术→管理，要走哪些路？](https://mp.weixin.qq.com/s/4_MB3L511h7qoXNxPRdOvQ)


---
# **视频教程推荐**

---
> [!NOTE]
>  对于学习编程的小伙伴来说，网上有海量现成的宝贵视频学习资源可供食用。
>  所以该板块会持续甄选和更新互联网上那些优秀的、值得反复观看的技术视频教程。
---

<!-- tabs:start -->

## 数据结构和算法

---
>  - **《郝斌 数据结构入门》**

<p align="center">
<iframe src="//player.bilibili.com/player.html?aid=6159200&bvid=BV11s41167h6&cid=10001692&page=1&as_wide=1&high_quality=1&danmaku=1" allowfullscreen="true" width="100%" height="450" scrolling="no" frameborder="0" sandbox="allow-top-navigation allow-same-origin allow-forms allow-scripts"></iframe>
</p>

---
>  - **《数据结构-浙江大学》**

<p align="center">
<iframe src="//player.bilibili.com/player.html?aid=18586085&bvid=BV1JW411i731&cid=30323260&page=1&as_wide=1&high_quality=1&danmaku=1" allowfullscreen="true" width="100%" height="450" scrolling="no" frameborder="0" sandbox="allow-top-navigation allow-same-origin allow-forms allow-scripts"></iframe>
</p>

---
>  - **《数据结构与算法基础（青岛大学-王卓）》**

<p align="center">
<iframe src="//player.bilibili.com/player.html?aid=82837069&bvid=BV1nJ411V7bd&cid=141710539&page=1&as_wide=1&high_quality=1&danmaku=1" allowfullscreen="true" width="100%" height="450" scrolling="no" frameborder="0" sandbox="allow-top-navigation allow-same-origin allow-forms allow-scripts"></iframe>
</p>



## 计算机网络

---
> - **《计算机网络微课堂》**

<p align="center">
<iframe src="//player.bilibili.com/player.html?aid=64605483&bvid=BV1c4411d7jb&cid=112161859&page=1&as_wide=1&high_quality=1&danmaku=1" allowfullscreen="true" width="100%" height="450" scrolling="no" frameborder="0" sandbox="allow-top-navigation allow-same-origin allow-forms allow-scripts"></iframe>
</p>

---
> - **《2019 王道考研 计算机网络》**

<p align="center">
<iframe src="//player.bilibili.com/player.html?aid=70228743&bvid=BV19E411D78Q&cid=121579556&page=1&as_wide=1&high_quality=1&danmaku=1" allowfullscreen="true" width="100%" height="450" scrolling="no" frameborder="0" sandbox="allow-top-navigation allow-same-origin allow-forms allow-scripts"></iframe>
</p>

---
> - **《韩立刚 计算机网络》**

<p align="center">
<iframe src="//player.bilibili.com/player.html?aid=415599826&bvid=BV1gV411h7r7&cid=262615706&page=1&as_wide=1&high_quality=1&danmaku=1" allowfullscreen="true" width="100%" height="450" scrolling="no" frameborder="0" sandbox="allow-top-navigation allow-same-origin allow-forms allow-scripts"></iframe>
</p>



---
## 操作系统

> - **《2020 南京大学  操作系统：设计与实现 (蒋炎岩) 》**

<p align="center">
<iframe src="//player.bilibili.com/player.html?aid=89733537&bvid=BV1N741177F5&cid=153258157&page=1&as_wide=1&high_quality=1&danmaku=1" allowfullscreen="true" width="100%" height="450" scrolling="no" frameborder="0" sandbox="allow-top-navigation allow-same-origin allow-forms allow-scripts"></iframe>
</p>

---
> - **《操作系统（哈工大李治军老师）》**

<p align="center">
<iframe src="//player.bilibili.com/player.html?aid=51437944&bvid=BV1d4411v7u7&cid=90016520&page=1&as_wide=1&high_quality=1&danmaku=1" allowfullscreen="true" width="100%" height="450" scrolling="no" frameborder="0" sandbox="allow-top-navigation allow-same-origin allow-forms allow-scripts"></iframe>
</p>

## 计算机组成原理

---
> - **《计算机组成原理（哈工大刘宏伟）》**

<p align="center">
<iframe src="//player.bilibili.com/player.html?aid=51650260&bvid=BV1t4411e7LH&cid=90407902&page=1&as_wide=1&high_quality=1&danmaku=1" allowfullscreen="true" width="100%" height="450" scrolling="no" frameborder="0" sandbox="allow-top-navigation allow-same-origin allow-forms allow-scripts"></iframe>
</p>

## 编译原理

---
> - **《编译原理（哈工大）》**

<p align="center">
<iframe src="//player.bilibili.com/player.html?aid=17649289&bvid=BV1zW411t7YE&cid=28812645&page=1&as_wide=1&high_quality=1&danmaku=1" allowfullscreen="true" width="100%" height="450" scrolling="no" frameborder="0" sandbox="allow-top-navigation allow-same-origin allow-forms allow-scripts"></iframe>
</p>

## 更多敬请期待...

---

更多的视频教程资源会持续更新，敬请期待...

<!-- tabs:end -->



---
# **编码之外**

---
> [!NOTE]
>  coding固然重要，但是兴趣爱好和个人生活才是此行的目的。走得慢才能走得远。
> 
>  **所做的一切是为了更丰富的体会和生活！**
---

## 生活&杂记

- [Findyi读者线下见面会](https://mp.weixin.qq.com/s?__biz=MzU3OTg2NDE0Mw==&mid=2247484095&idx=1&sn=81fdcf05da45897cd4a8ea48648c35d2&chksm=fd5ed19fca295889fce7ce5e5771dbca14a4858106011078fc632e4ec549299465cc197b097e&token=2137302191&lang=zh_CN#rd)
- [耗子叔直播Review](https://mp.weixin.qq.com/s?__biz=MzU3OTg2NDE0Mw==&mid=2247484191&idx=1&sn=7ea2c76f991ae4590f0ffd5f5421349a&chksm=fd5ed03fca295929f38d5c63e2b8934e95397581bc636b78ef38cc444c0d87686c81d6d6c627&token=2137302191&lang=zh_CN#rd)
- [谈谈我的爱情观](https://mp.weixin.qq.com/s?__biz=MzU3OTg2NDE0Mw==&mid=2247484173&idx=1&sn=48ed476b881b0df72d2dad1905122e50&chksm=fd5ed02dca29593b78b2ff87e2aa1d70bbe8fbeb0390f12e3b5b57b9b29055dfd18496e45422&token=2137302191&lang=zh_CN#rd)
- [阿巩的2021年终复盘](https://mp.weixin.qq.com/s?__biz=MzU3OTg2NDE0Mw==&mid=2247484297&idx=1&sn=1f977200254c074a462fa49e527979c7&chksm=fd5ed0a9ca2959bfeed6fb39c5f405ba154e5aef542e53eff01740ca3bd48c94216df27c84f4&token=2137302191&lang=zh_CN#rd)

# **联系作者**
---

## 关于作者

- 后端程序猿(媛)，19年毕业后从事了IT行业，做过运维，用Python写过游戏服务器、爬虫和Web后端；去年转做了Go语言，至今以Golang为主，现从事微服务开发相关工作。

- 热爱参加技术分享和社群活动，比起一个人闷头研究，更乐于和开发者们思维碰撞；在向大家汲取知识和宝贵经验的同时，也在积极将自己的所学所想回馈给读者。
- 去年从北漂变成了沪漂，不论身在何方我对技术和知识探索的热情是分毫不减的。
- 日拱一卒，欢迎大家的关注和订阅！


## 微信联系

<p align="left">
    <a href="http://mp.weixin.qq.com/s?__biz=MzU3OTg2NDE0Mw==&mid=2247484799&idx=1&sn=249160dd75cb9c5ddd81c67176859ab8&chksm=fd5ed65fca295f49f82fe59858b7603808040d81b3cd19c1d7fe601208b937e1c61bd406db98&scene=18#wechat_redirect" target="_blank">
        <img src="image/qrcode_for_gh_70b75f5f3e79_258.jpg" width=""/>
    </a>
</p>


---
# **本站开源地址**
---

## Gitlab开源地址

- [https://gitlab.com/893376179/cqcoding](https://gitlab.com/893376179/cqcoding) **欢迎Star支持**


---
# 持续更新中...

网站内容会持续保持更新，欢迎收藏品鉴！